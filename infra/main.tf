resource "random_integer" "ri" {
  min = 10000
  max = 99999
}

resource "azurerm_resource_group" "rg" {
  name     = "rg-ci-cd-web-site-nounou-bloonie-antho-${var.environment}"
  location = var.DEPLOY_REGION
}

resource "azurerm_service_plan" "YourBOX_App_Service_Plan" {
  name                = "yourbox-app-service-plan-${var.environment}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_virtual_network" "vnet" {
  name                = "yourbox-vnet-${var.environment}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "mysql_subnet" {
  name                 = "mysql-subnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_private_dns_zone" "my-private_dns_zone" {
  name                = "${var.environment}.mysql.database.azure.com"
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_private_dns_zone_virtual_network_link" "my_virtual_network_link" {
  name                  = "my_virtual_network_link"
  private_dns_zone_name = azurerm_private_dns_zone.my-private_dns_zone.name
  virtual_network_id    = azurerm_virtual_network.vnet.id
  resource_group_name   = azurerm_resource_group.rg.name
}

resource "azurerm_mysql_flexible_server" "MySQL_server" {
  name                        = "yourbox-mysql-server-${var.environment}"
  location                    = azurerm_resource_group.rg.location
  resource_group_name         = azurerm_resource_group.rg.name

  administrator_login         = var.username_DB_SQL
  administrator_password      = var.password_DB_SQL
  sku_name                    = "GP_Standard_D2ds_v4"
  version                     = "8.0.21"

  delegated_subnet_id = azurerm_subnet.mysql_subnet.id
  public_network_access_enabled = true
  private_dns_zone_id = azurerm_private_dns_zone.my-private_dns_zone.id

}

resource "azurerm_mysql_flexible_database" "MySQL_database" {
  name                       = "yourbox-mysql-database-${var.environment}"
  resource_group_name         = azurerm_resource_group.rg.name
  server_name                 = azurerm_mysql_flexible_server.MySQL_server.name
  charset                     = "utf8"
  collation                   = "utf8_general_ci"
}

resource "azurerm_linux_web_app" "YourBOX_app" {
  name                = "isima-yourbox-app-${var.environment}"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location
  service_plan_id     = azurerm_service_plan.YourBOX_App_Service_Plan.id

  site_config {
    application_stack {
      docker_image_name = "${var.docker_registry_username}/${var.docker_image_app_name}:${var.docker_image_tag}"
      docker_registry_url = var.docker_registry_url
      docker_registry_username = var.docker_registry_username
      docker_registry_password = var.docker_registry_password

    }
  }
  app_settings = {
    "WEBSITES_ENABLE_APP_SERVICE_STORAGE" = "false"
    "DOCKER_REGISTRY_SERVER_URL" = var.docker_registry_url
    "DOCKER_REGISTRY_SERVER_USERNAME" = var.docker_registry_username
    "DOCKER_REGISTRY_SERVER_PASSWORD" = var.docker_registry_password
    "DOCKER_CUSTOM_IMAGE_NAME" = "${var.docker_registry_username}/${var.docker_image_app_name}:${var.docker_image_tag}"
    "WEBSITES_PORT" = "80"
  }

}



