output "azure_rg_name" {
  value = azurerm_resource_group.rg.name
}

output "app_service_url" {
  value = azurerm_linux_web_app.YourBOX_app.default_hostname
}

output "app_service_name" {
  value = azurerm_linux_web_app.YourBOX_app.name
}

output "sql_server_fully_qualified_domain_name" {
  value = azurerm_mysql_flexible_server.MySQL_server.fqdn
}

output "mail_server" {
  value = var.mail_server
}

output "mail_username" {
  value = var.mail_username
}

output "mail_password" {
  value = var.mail_password
  sensitive = true
}

output "sql_server_name" {
  value = azurerm_mysql_flexible_server.MySQL_server.name
}

output "db_user" {
  value = azurerm_mysql_flexible_server.MySQL_server.administrator_login
}

output "db_pass" {
  value = azurerm_mysql_flexible_server.MySQL_server.administrator_password
  sensitive = true
}

output "db_port" {
  value = "3306" # Default port for Azure MySQL Database
}

output "resource_group_location" {
  value = azurerm_resource_group.rg.location
}
