variable "client_id" {
  type = string
}

variable "client_secret" {
  type = string
}

variable "tenant_id" {
  type = string
}

variable "subscription_id" {
  type = string
}

variable "environment" {
  type = string
}

variable "DEPLOY_REGION" {
    type = string
}

variable "username_DB_SQL" {
  type = string
}

variable "password_DB_SQL" {
  type = string
}

variable "mail_server" {
  type = string
}
variable "mail_username" {
    type = string
}

variable "mail_password" {
  type = string
}

variable "project_root" {
  type = string
}

variable "docker_image_namespace" {
  description = "The namespace where the docker image is stored"
  type        = string
}

variable "docker_image_app_name" {
  description = "The name of the docker image to use"
  type        = string
}

variable "docker_image_tag" {
    description = "The tag of the docker image to use"
    type        = string
}

variable "docker_registry_url" {
  description = "FQDN of the registry"
  type = string
}

variable "docker_registry_username" {
  description = "Username to connect to the docker registry"
  type = string
}

variable "docker_registry_password" {
  description = "Password to connect to the docker registry"
  type = string
}