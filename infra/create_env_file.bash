#!/bin/bash

# Fetch output from Terraform
outputs=$(cat "$1")

# Parse output from Terraform
app_service_url=$(echo "$outputs" | jq -r '.app_service_url.value')
sql_server_fqdn=$(echo "$outputs" | jq -r '.sql_server_fully_qualified_domain_name.value')
sql_database_name=$2
db_port=$(echo "$outputs" | jq -r '.db_port.value')
db_user=$(echo "$outputs" | jq -r '.db_user.value')
db_pass=$(echo "$outputs" | jq -r '.db_pass.value')
mail_pass=$(echo "$outputs" | jq -r '.mail_password.value')
mail_user=$(echo "$outputs" | jq -r '.mail_username.value')
mail_server=$(echo "$outputs" | jq -r '.mail_server.value')

ENV_FILE_PATH="/var/www/html/yourbox/.env.php"

# Create .env.php file
cat <<EOF > "$ENV_FILE_PATH"
<?php
putenv("PROJECT_ROOT=/var/www/html/yourbox/");
putenv("DB_PORT=$db_port");
putenv("DB_host=$sql_server_fqdn");
putenv("DB_name=$sql_database_name");
putenv("DB_user=$db_user");
putenv("DB_pass=$db_pass");
putenv("MAIL_pass=$mail_pass");
putenv("MAIL_host=$mail_server");
putenv("MAIL_user=$mail_user");
putenv("URL_root=https://$app_service_url/");
?>
EOF

echo ".env.php file created successfully."