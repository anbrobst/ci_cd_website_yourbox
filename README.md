# YourBox : la future révolution du streaming 
**Bientôt un concurrent pour YouTube et Netflix ??????**

YourBox est une application de streaming web permettant de partager et de visionner des vidéos, mais aussi des photos, gifs... Elle offre une plateforme aux utilisateurs pour uploader, rechercher et visionner des vidéos.

## Fonctionnalités

- **Upload de vidéos :** Les utilisateurs peuvent partager leurs propres vidéos sur la plateforme sous réserve qu'elles respectent nos conditions d'utilisations.
  
- **Fonctionnalité de recherche :** Une fonction de recherche efficace pour trouver des vidéos basées sur des mots-clés. Le surlignage des mots-clés lors de la recherche permet une meilleure compréhension et permet de trouver plus facilement ce que l'on recherche. Pour éviter les surcharges du serveur, notre fonction de recherche stockera sur votre ordinateur vos termes recherchés dans un fichier cache?
   
- **Authentification des utilisateurs :** Système d'authentification des utilisateurs pour gérer les comptes et accéder aux fonctionnalités. La majorité des fonctionnalitées du site ne sont disponibles qu'uniquement après inscription et authentification de l'utilisateur (visionnage des vidéos, commentaires, upload, interaction, ...). Les utilisateurs auront également la possibilité de modifier certaines informations de leur profil sur la plateforme.

- **Pagination :** Affichage paginé des vidéos sur la page d'acceuil pour une meilleure navigation.
  
- **Interactions utilisateurs :** Les utilisateurs ont la possibilités de commenter et de liker les vidéos ou commentaires de la plateforme

## Remarques 

### Quelques fonctionnalités manquantes ... Pour l'instant!!
Nous tenons à vous informer que notre site est actuellement en cours de développement et n'a pas encore atteint son état final. En raison de contraintes de temps, certaines fonctionnalités n'ont pas encore été complètement implémentées. Vous pourriez rencontrer des boutons ou des liens qui ne mènent nulle part ou qui ne semblent pas encore fonctionnels, tels que les boutons VIP, Challenge, et les abonnés qui restent fixes sur la page de profil.

Nous tenons à vous rassurer que ces éléments font partie de nos plans futurs, et nous prévoyons de finaliser le projet avec toutes les fonctionnalités prévues. Cependant, pour le moment, notre priorité est de nous concentrer sur l'intégration continue du développement, afin de vous offrir une expérience utilisateur plus stable et optimale.

### Attention lors de la conexion sur notre site
Nous souhaitons vous informer qu'actuellement, notre site rencontre des difficultés avec certains services de messagerie, en particulier les services de messagerie très sécurisés tels que Gmail. L'utilisation d'adresses e-mail de ce type peut entraîner des problèmes de fonctionnement et des bugs sur notre plateforme.

Afin d'optimiser votre expérience sur notre plateforme, nous vous recommandons vivement d'utiliser un client mail proposé avec une sécurité minimale comme celui de l'hébergeur: Dyjix. Actuellement, notre site fonctionne de manière plus fluide avec des services d'hébergement spécifiques, et l'utilisation de Dyjix contribuera à éviter les éventuels problèmes de compatibilité.

## Table des matières

- [YourBox : la future révolution du streaming](#yourbox--la-future-révolution-du-streaming)
  - [Fonctionnalités](#fonctionnalités)
  - [Remarques](#remarques)
    - [Quelques fonctionnalités manquantes ... Pour l'instant!!](#quelques-fonctionnalités-manquantes--pour-linstant)
    - [Attention lors de la conexion sur notre site](#attention-lors-de-la-conexion-sur-notre-site)
  - [Table des matières](#table-des-matières)
  - [Pour commencer](#pour-commencer)
    - [Prérequis](#prérequis)
    - [Installation et configuration](#installation-et-configuration)
    - [Utilisation](#utilisation)
    - [Contribution](#contribution)

## Pour commencer

Ces instructions vous aideront à configurer et à exécuter YourBox.

### Prérequis

- Abonnement Azure et Compte de service avec le rôle [Contributor](https://docs.microsoft.com/en-us/azure/role-based-access-control/media/rbac-roles/azure-rbac-built-in-roles.png)
  ![Compte de service](Docs/Compte_de_service_Azure.png)

  Pensez à noter : application_id, tenant_id, secret

- Serveur mail (par exemple [Dyjix](https://dyjix.eu/autres/edu/))
  
- [Git Bash](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git) installé sur votre machine
  
- Base de données MySQL autre que sur Azure pour faire tourner les tests d'intégration

### Installation et configuration

1. ***Clonez le dépôt*** 

   **En raison d'une très grande difficulté pour configurer le projet en local, seul la configuration de l'application par la pipeline et Terraform et le lancement avec Azure est pris en charge pour le moment.** Nous vous conseillons donc dans un premier temps de cloner le dépôt GitLab sur votre machine. Pour cela, ouvrez un terminal et exécutez la commande suivante :
   
   ```bash
   git clone https://gitlab.isima.fr/anbrobst/ci_cd_website_yourbox.git
   ```
    1.1 Vous avez ainsi la possibilité de supprimer le **.git** dans le dossier racine 

    1.2  Ensuite, créer un nouveau dépôt Git sur GitLab et mettez y le dossier racine du projet (sans le .git)

2. ***Configurez les variables d'environnement de votre dépot*** 
    
    Pour cela, définissez les variables suivantes dans vos paramètres CI_CD:


    | Variables CI/CD                          | Description                          | Environnement        |
    | :--------------------------------------- |:------------------------------------:| --------------------:|
    | **AZURE_SQL_DATABASE_NAME**                         | Nom de la base de données MySQL sur Azure                 | staging / production                  |
    | **AZURE_SQL_SERVER_FQDN**                         | Fully Qualified Domain Name (adresse IP) du serveur MySQL sur Azure                 | staging / production                  |
    | **DB_PASS**                         | Mot de passe de la base de données MySQL pour les tests d'intégration lancé par la pipeline                  | test                  |
    | **DB_USER**                         | Nom d'utilisateur de la base de données MySQL pour les tests d'intégration lancé par la pipeline                  | test                  |
    | **DOCKER_REGISTRY**                      | Registre docker pour stocker votre image ( publique comme docker.io ou privé comme docker.isima.fr)                  | all                  |
    | **GITLAB_ACCESS_TOKEN**                  | [Jeton d'accès avec des droits en écriture/lecture généré sur GitLab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)     | all                  |
    | **PROJECT_ROOT**                         | Chemin absolu dans le dépôt GitLab vers le dossier yourbox/ pour les tests d'intégration lancé par la pipeline (ex: /builds/anbrobst/ci_cd_website_yourbox/yourbox/)                  | test                  |
    | **STATE_NAME**                           | Nom du fichier tfstate généré par Terraform               | staging / production |
    | **TEST_SQL_DATABASE_NAME**                         | Nom de la base de données MySQL pour les tests                  | test                  |
    | **TEST_SQL_SERVER_FQDN**                         | Fully Qualified Domain Name (adresse IP) du serveur où est configuré la base de données MySQL pour les tests                 | test                  |
    | **TF_VAR_DEPLOY_REGION**                 | Région de déploiement des ressources dans Azure (ex: France Central) | staging / production |
    | **TF_VAR_client_id**                     | Id du compte de service généré dans Azure                  | all                  |
    | **TF_VAR_client_secret**                 | Mot de passe du compte de service généré dans Azure                 | all                  |
    | **TF_VAR_docker_image_app_name**         | Nom de l'application dans l'image Docker (ex: yourbox_webapp_staging) | staging / production |
    | **TF_VAR_docker_image_namespace**        | Namespace pour classifier l'image Docker dans le registre sous la forme : <url/username> (ex: docker.isima.fr/anbrobst)     | all                  |
    | **TF_VAR_docker_registry_password**      | Mot de passe pour la connexion au registre Docker       | all                  |
    | **TF_VAR_docker_registry_url**           | URL du registre Docker sous la forme https://<registre>  | all                  |
    | **TF_VAR_docker_registry_username**      | Nom d'utilisateur pour la connexion au registre Docker  | all                  |
    | **TF_VAR_environment**                   | Environnement du code ( staging ou production)                | staging / production |
    | **TF_VAR_mail_password**                 | Mot de passe du serveur de messagerie                      | all                  |
    | **TF_VAR_mail_server**                   | Adresse URL du serveur de messagerie     | all                  |
    | **TF_VAR_mail_username**                 | Nom d'utilisateur du serveur de messagerie            | all                  |
    | **TF_VAR_password_DB_SQL**               | Mot de passe de la base de données MySQL             | all                  |
    | **TF_VAR_project_root**                  | Chemin absolue dans le Docker vers le dossier yourbox/ (ex: /var/www/html/yourbox/)   | all                  |
    | **TF_VAR_subscription_id**               | Id de l'abonnement Azure           | all                  |
    | **TF_VAR_tenant_id**                     | Id de connexion du tenant généré par la création du compte de service Azure           | all                  |
    | **TF_VAR_username_DB_SQL**               | Nom d'utilisateur de la base de données MySQL             | all                  |
    
    
    
### Utilisation

1. ***Lancez la pipeline*** 

   * Une fois les variables d'environnement configurées, lancez la pipeline   sur votre dépôt GitLab. Cela créera les ressources nécessaires dans Azure grâce à Terraform et déployera l'application contenu dans une image Docker sur le serveur web. L'application existera alors en deux fois : une version de staging et une version de production.

2. ***Accédez à l'application*** 

     - Il ne vous restera plus qu'à vous rendre sur l'url Azure avec le bon environnement pour accéder à l'application :
  
       `
            https://isima-yourbox-app-<TF_VAR_environment>.azurewebsites.net/
       `

### Contribution

- [Nourrane MALLEPEYRE](https://gitlab.isima.fr/nomallepey)
- [Benjamin MICHEL](https://gitlab.isima.fr/bemichel5)
- [Anthony BROBST](https://gitlab.isima.fr/anbrobst)

         
    
    
    
    
    
    
    
    
    
    


