<?php

require_once __DIR__ . '/../YourBOX/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable( __DIR__ . '/../YourBOX','.env.test');
$dotenv->load();
$dotenv->required(['DB_host','PROJECT_ROOT', 'DB_host', 'DB_name', 'DB_user', 'DB_pass'])->notEmpty();

$projectRoot = getenv('PROJECT_ROOT');
$dbHost = getenv('DB_host');
$dbName = getenv('DB_name');
$dbUser = getenv('DB_user');
$dbPass = getenv('DB_pass');
