<?php

namespace integration;
require_once __DIR__ . "/../../YourBOX/.env.php";
use PDO;
use PHPUnit\Framework\TestCase;
/**
 * Classe de tests pour l'authentification (login, registration)
 *
 * @covers login, registration
 * Plan de test :
 * 1. testUserRegistration : Tester la création d'un utilisateur dans la base de données
 * 2. testUnsuccessfulAuthentication : Tester une authentification invalide
 * 3. testSuccessfulAuthentication : Tester une authentification valide
 * 4. testUserSuppr : Tester la suppression d'un utilisateur dans la base de données
 */
class AuthenticationTest extends TestCase
{
    private $db;

    /**
     * Initialisation avant chaque test
     *
     * @throws \Exception
     */
    protected function setUp(): void
    {
        $dbHost = getenv('DB_host');
        $dbName = getenv('DB_name');
        $dbUser = getenv('DB_user');
        $dbPass = getenv('DB_pass');
        // Initialisation de la bdd
        $this->db = new PDO("mysql:host=".$dbHost.";DB_name=".$dbName, $dbUser,$dbPass);
    }

    /**
     * Nettoyage après chaque test
     */
    protected function tearDown(): void
    {
        $this->db = null;
    }

    /**
     * Teste la création d'un utilisateur dans la base de données
     *
     * @throws \Exception
     */
    public function testUserRegistration(){
        $userData = [
            'username' => 'Christophe_Tilmant',
            'password' => password_hash('password123', PASSWORD_BCRYPT),
            'email' => "CT@gmail.com",
            'gender' => "man",
            'confirmation_token' => bin2hex(random_bytes(30)),
        ];
        $req = $this->db->prepare("INSERT INTO YourBOX.users (username, password, email, gender, confirmation_token) VALUES (?, ?, ?, ?, ?)");
        $result = $req->execute(array_values($userData));
        $this->assertTrue($result);
    }

    /**
     * Teste une authentification invalide
     */
    public function testUnsuccessfulAuthentication()
    {
        $userData = [
            'username' => 'Christophe_Tilmant',
            'password' => 'WrongPassword',
        ];

        $req = $this->db->prepare("SELECT * FROM YourBOX.users WHERE username = ?");
        $req->execute([$userData['username']]);
        $user = $req->fetch();
        $this->assertFalse(password_verify($userData['password'], $user -> password));
    }

    /**
     * Teste une authentification valide
     */
    public function testSuccessfulAuthentication()
    {
        $userData = [
            'username' => 'Christophe_Tilmant',
            'password' => 'password123',
        ];
        $req = $this->db->prepare("SELECT * FROM YourBOX.users WHERE username = ?");
        $req->execute([$userData['username']]);
        $user = $req->fetch(PDO::FETCH_ASSOC);
        $this->assertTrue(password_verify($userData['password'], $user['password']));
    }

    /**
     * Teste la suppression d'un utilisateur dans la base de données
     */
    public function testUserSuppr(){
        $req = $this->db->prepare("DELETE FROM YourBOX.users WHERE username = 'Christophe_Tilmant'");
        $result = $req->execute();
        $this->assertTrue($result);
    }
}