<?php

namespace integration;
require_once __DIR__ . "/../../YourBOX/.env.php";
require_once getenv('PROJECT_ROOT') . 'src/inc/functions.php';
use PDO;
use PHPUnit\Framework\TestCase;
/**
 * @covers video, upload, deletion
 * Plan de test :
 * 1. testAddVideo : Tester l'ajout d'une video dans la bdd
 * 2. testVideoSuppr : Tester la suppression d'une video de la bdd
 */
class VideoTest extends TestCase
{
    private $db;

    protected function setUp(): void
    {
        $dbHost = getenv('DB_host');
        $dbName = getenv('DB_name');
        $dbUser = getenv('DB_user');
        $dbPass = getenv('DB_pass');
        // Initialisation de la bdd
        $this->db = new PDO("mysql:host=" . $dbHost . ";DB_name=" . $dbName, $dbUser, $dbPass);
    }

    protected function tearDown(): void
    {
        $this->db = null;
    }

    public function testAddVideo(): void
    {
        $file_path = str_replace('/YourBOX/', '', getenv('PROJECT_ROOT')) . '/tests/integration/video_sample/epb-sonic-vs-rainbow-dash.mp4';
        $thumbnail = str_replace('/YourBOX/', '', getenv('PROJECT_ROOT')) . '/tests/integration/video_sample/epb-sonic-vs-rainbow-dash.jpg';
        $video_attributes = _get_video_attributes($file_path);

        echo "\nadding video:" . $file_path ."\n";

        $video_data = [
            'id' => 1,
            'name' => 'Epic-Pixel-Battle-Sonic-VS-Rainbow-Dash',
            'complete_name' => 'epb-sonic-vs-rainbow-dash.mp4',
            'file_url' => $file_path,
            'size' => filesize($file_path),
            'duration' => $video_attributes['duration'],
            'URL_preview' => $thumbnail,
            'resolution' => $video_attributes['resolution'],
            'sender_id' => 3,
            'description' => "This is a test video",
            'creator' => "Epic Pixel Battle",
            'main_category' => "Music",
        ];
        $req = $this->db->prepare('INSERT INTO YourBOX.files (id, name, complete_name, file_url, size, duration, date_upload, URL_preview, resolution, sender_id, description, creator, main_category) VALUES(?,?,?,?,?,?,NOW(), ?, ?, ?, ?, ?, ?)');
        $result = $req->execute(array_values($video_data));
        $this->assertTrue($result);
    }

    public function testVideoSuppr(){
        echo "\ndeleting video\n";
        $req = $this->db->prepare("DELETE FROM YourBOX.files WHERE id = 1");
        $result = $req->execute();
        $this->assertTrue($result);
    }
}
?>