<?php
require_once __DIR__ . "/../../YourBOX/.env.php";
require_once getenv('PROJECT_ROOT').'src/inc/Plugins/Comments.php';

use PHPUnit\Framework\TestCase;
use Plugins\Comments;

/**
 * Classe de tests pour la classe Comments
 *
 * @coversDefaultClass \Plugins\Comments
 * Plan de test :
 * 1. Tester la création d'un commentaire.
 * 2. Tester la récupération de commentaires sans réponses.
 * 3. Tester l'enregistrement de deux réponses à un commentaire.
 * 4. Tester la suppression d'un commentaire avec ses réponses.
 */
class CommentsTest extends TestCase
{
    private PDO $db;
    private Array $commentData;
    private Comments $comments;

    /**
     * Insertion d'un commentaire dans la base de données
     *
     * @param array $data Données du commentaire à insérer
     * @return void
     */
    public function InsertIntoBDD($data): void
    {
        $reqInsert = $this->db->prepare("INSERT INTO YourBOX.comments SET username= ?, email = ?,  content = ?, ref_id = ?, ref = ?, created = ?, parent_id = ? ");
        $reqInsert->execute([$data['username'], $data['email'], $data['content'], $data['ref_id'], $data['ref'], $data['created'], $data['parent_id']]);
    }

    /**
     * Initialisation avant chaque test
     *
     * @return void
     */
    protected function setUp(): void
    {
        $dbHost = getenv('DB_host');
        $dbName = getenv('DB_name');
        $dbUser = getenv('DB_user');
        $dbPass = getenv('DB_pass');
        // Initialisation de la bdd
        $this->db = new PDO("mysql:host=".$dbHost.";DB_name=".$dbName, $dbUser,$dbPass);
        $this->comments = new Comments($this->db);

        $this->commentData = [
            'username' => 'Lucas Pastor',
            'email' => 'lucasPastor@gmail.com',
            'content' => "First!!!",
            'created' =>  date('Y-m-d H:i:s'),
            'ref' => 'test_files',
            'ref_id' => -2,
            'parent_id' => 0

        ];
    }

    /**
     * Teste la création d'un commentaire avec des données valides
     *
     * @return void
     */
    public function testSaveCommentWithValidData()
    {
        $this->InsertIntoBDD($this->commentData);

        $reqFetch=$this->db->prepare("SELECT * FROM YourBOX.comments WHERE ref_id = :ref_id AND ref= :ref ORDER BY created DESC" );

        $reqFetch->execute(['ref_id' => $this->commentData['ref_id'], 'ref' =>$this->commentData['ref']]);
        $comments = $reqFetch->fetchAll(PDO::FETCH_OBJ);
        // Ensure that at least one comment is fetched
        $this->assertNotEmpty($comments, 'No comments found');

        $idComment = $comments[0]->id;
        echo "id of the comment created:".$idComment."\n";

        $this->assertEquals($this->commentData['content'], $comments[0]->content);

        echo "Inserted comment content: " . $this->commentData['content'] . PHP_EOL;
        echo "Fetched comment content: " . $comments[0]->content . PHP_EOL;
    }

    /**
     * Teste la récupération de commentaires sans réponses
     *
     * @return void
     */
    public function testFindAllWithNoReply()
    {
        $result = $this->comments->findAll("test", -10);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    /**
     * Teste l'ajout de deux réponses à un commentaire
     *
     * @return void
     */
    public function testReplyToAComment()
    {
        // on recupère l'id du commentaire precedement créé
        $reqFetch=$this->db->prepare("SELECT * FROM YourBOX.comments WHERE ref_id = :ref_id AND ref= :ref ORDER BY created DESC" );
        $reqFetch->execute(['ref_id' => $this->commentData['ref_id'], 'ref' =>$this->commentData['ref']]);
        $comments = $reqFetch->fetchAll(PDO::FETCH_OBJ);

        $this->assertNotEmpty($comments, 'No comments found');

        $ReplyCommentData1 = [
            'username' => 'Marguerite Yourcenar',
            'email' => 'MargueriteYourcenar@gmail.com',
            'content' => "Second!!!",
            'created' =>  date('Y-m-d H:i:s'),
            'ref' => $this->commentData['ref'],
            'ref_id' => -2,
            'parent_id' => $comments[0]->id ,
        ];
        $ReplyCommentData2 = [
            'username' => 'Empereur Caligula',
            'email' => 'EmpereurCaligula@gmail.com',
            'content' => "Third!!!",
            'created' =>  date('Y-m-d H:i:s'),
            'ref' => $this->commentData['ref'],
            'ref_id' => -2,
            'parent_id' => $comments[0]->id,
        ];

        $this->InsertIntoBDD($ReplyCommentData1);
        $this->InsertIntoBDD($ReplyCommentData2);

        $result = $this->comments->findAll($this->commentData['ref'], -2);
        $this->assertIsArray($result);
        $this->assertNotEmpty($result, 'The result array is empty');

        $this->assertCount(1, $result);

        foreach ($result as $comment){
            $this->assertCount(2, $comment->replies);
        }
    }

    /**
     * Teste la suppression d'un commentaire avec ses réponses
     *
     * @return void
     */
    public function testDeleteWithReplies()
    {
        $reqFetch= $this->db->prepare("SELECT * FROM YourBOX.comments WHERE ref_id = :ref_id AND ref= :ref ORDER BY created DESC" );
        $reqFetch->execute(['ref_id' => $this->commentData['ref_id'], 'ref' => $this->commentData['ref']]);
        $comments = $reqFetch->fetchAll(PDO::FETCH_OBJ);

        $idComment = $comments[0]->id;

        $result = $this->comments->delete($idComment,  $comments[0]->ref,  $comments[0]->ref_id);
        echo "id of the comment to delete:".$idComment."\n";

        $reqFetch=$this->db->prepare("SELECT * FROM YourBOX.comments WHERE ref_id =-2 AND ref= :ref ORDER BY created DESC" );
        $reqFetch->execute(['ref' => $this->commentData['ref']]);

        $deletedComments = $reqFetch->fetchAll(PDO::FETCH_OBJ);
        $this->assertEmpty($deletedComments, 'Comment was not deleted');
    }
}
