<?php

require_once getenv('PROJECT_ROOT') . 'src/inc/Plugins/PaginatedQuery.php';

use Plugins\PaginatedQuery;
use PHPUnit\Framework\TestCase;


/**
 * @coversDefaultClass \Plugins\PaginatedQuery
 * Plan de test :
 * Base toi sur les méthodes ecrit en bas
 * 1. testGetPagesWithResults : Tester le nombre de pages retournées avec des résultats
 * 2. testGetPagesWithNoResults : Tester le nombre de pages retournées sans résultats
 * 3. testGetCurrentPageDefault : Tester la page courante par défaut
 * 4. testGetCurrentPageSpecific : Tester la page courante spécifique
 * 5. testGetCurrentPageInvalid : Tester la page courante invalide
 * 6. testGetItemsValidPage : Tester la récupération des éléments avec une page valide
 * 7. testGetItemsInvalidPage : Tester la récupération des éléments avec une page invalide
 * 8. testPreviousLinkOnFirstPage : Tester le lien précédent sur la première page
 * 9. testPreviousLinkOnSecondPage : Tester le lien précédent sur la deuxième page
 * 10. testNextLinkOnLastPage : Tester le lien suivant sur la dernière page
 * 11. testNextLinkOnFirstPage : Tester le lien suivant sur la première page
 * 12. testGetItemsPageDoesNotExist : Tester la récupération des éléments avec une page qui n'existe pas
 * 13. testGetCurrentPageInvalidNumber : Tester la récupération de la page courante avec un numéro invalide
 * 14. testGetCurrentPageNegativeNumber : Tester la récupération de la page courante avec un numéro négatif
 * 15. testGetPagesCountQueryFails : Tester la récupération du nombre de pages avec une requête qui échoue
 * 16. testGetItemsQueryFails : Tester la récupération des éléments avec une requête qui échoue
 */
#[AllowDynamicProperties] final class PaginatedQueryTest extends TestCase
{
    private PDO $pdo;
    private PDOStatement $statement;
    private int $ElementsPerPage = 5;

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        $this->pdo = $this->createMock(PDO::class);
        $this->statement = $this->createMock(PDOStatement::class);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function __construct(string $name)
    {
        parent::__construct($name);
        $this->setUp();
    }



    /**
     * @throws Exception
     */
    public function testGetPagesWithResults()
    {
        global $dbName;
        $this->statement->method('fetch')
            ->willReturn([10]); // Simule 10 enregistrements dans la base de données
        $this->pdo->method('query')
            ->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertEquals(2, $paginatedQuery->getPages()); // 10 enregistrements, 5 par page, donc 2 pages
    }

    public function testGetPagesWithNoResults()
    {
        global $dbName;
        $this->statement->method('fetch')
            ->willReturn([0]); // Simule 0 enregistrements dans la base de données
        $this->pdo->method('query')
            ->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertEquals(0, $paginatedQuery->getPages()); // 0 enregistrements, donc 0 pages
    }

    /**
     * @throws Exception
     */
    public function testGetCurrentPageDefault()
    {
        global $dbName;
        $_GET['page'] = null;

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertEquals(1, $paginatedQuery->getCurrentPage()); // Par défaut, la page devrait être 1
    }

    /**
     * @throws Exception
     */
    public function testGetCurrentPageSpecific()
    {
        global $dbName;
        $_GET['page'] = 3;

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertEquals(3, $paginatedQuery->getCurrentPage()); // Test avec la page spécifiée
    }

    public function testGetCurrentPageInvalid()
    {
        global $dbName;
        $this->expectException(Exception::class); // S'attend à une exception
        $_GET['page'] = -1; // Valeur invalide

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $paginatedQuery->getCurrentPage();
    }

    /**
     * @throws Exception
     */
    public function testGetItemsValidPage()
    {
        global $dbName;
        $_GET['page'] = 1;
        $this->statement->method('fetch')
            ->willReturn([10]); // Simule 10 enregistrements
        $this->pdo->method('query')
            ->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertInstanceOf(PDOStatement::class, $paginatedQuery->getItems());
    }

    public function testGetItemsInvalidPage()
    {
        global $dbName;
        $this->expectException(Exception::class); // S'attend à une exception pour une page invalide
        $_GET['page'] = 3; // Page invalide (suppose qu'il n'y a que 2 pages)

        // Mock de la méthode query pour retourner un PDOStatement mock
        $this->pdo->method('query')->willReturn($this->statement);

        // Mock de la méthode fetch pour retourner false
        $this->statement->method('fetch')->willReturn(false);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $paginatedQuery->getItems();
    }

    /**
     * @throws Exception
     */
    public function testPreviousLinkOnFirstPage()
    {
        global $dbName;
        $_GET['page'] = 1;

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertNull($paginatedQuery->previousLink("link"));
    }

    /**
     * @throws Exception
     */
    public function testPreviousLinkOnSecondPage()
    {
        global $dbName;
        $_GET['page'] = 2;

        $expected = '<a class="btn-page btn-primary" href="link?page=1">&laquo; Previous page</a>';

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $actual = $paginatedQuery->previousLink('link');

        $this->assertEquals(trim($expected), trim($actual));

    }

    /**
     * @throws Exception
     */
    public function testNextLinkOnLastPage()
    {
        global $dbName;
        $_GET['page'] = 2; // Supposons qu'il y ait seulement 2 pages
        $this->statement->method('fetch')
            ->willReturn([10]); // Simule 10 enregistrements
        $this->pdo->method('query')
            ->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->assertNull($paginatedQuery->nextLink("link"));
    }

    /**
     * @throws Exception
     */
    public function testNextLinkOnFirstPage()
    {
        global $dbName;
        $_GET['page'] = 1; // Première page
        $this->statement->method('fetch')
            ->willReturn([10]); // Simule 10 enregistrements
        $this->pdo->method('query')
            ->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $expected = '<a class="btn-page btn-primary" href="link?page=2"> Next page &raquo;</a>';
        $actual = $paginatedQuery->nextLink('link');

        $this->assertEquals(trim($expected), trim($actual));
    }

    public function testGetItemsPageDoesNotExist()
    {
        global $dbName;
        $this->statement->method('fetch')->willReturn(10); // Supposons qu'il y ait 10 éléments au total
        $this->pdo->method('query')->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("This page doesn't exist");

        $_GET['page'] = 5; // Une page qui n'existe pas
        $paginatedQuery->getItems();
    }

    public function testGetCurrentPageInvalidNumber()
    {
        global $dbName;
        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("The page number is not valid");

        $_GET['page'] = 'invalid'; // Un numéro de page non valide
        $paginatedQuery->getCurrentPage();
    }

    public function testGetCurrentPageNegativeNumber()
    {
        global $dbName;
        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->expectException(Exception::class);
        $this->expectExceptionMessage("The page number is not valid");

        $_GET['page'] = -1; // Un numéro de page négatif
        $paginatedQuery->getCurrentPage();
    }

    public function testGetPagesCountQueryFails()
    {
        global $dbName;
        $this->statement->method('fetch')->willReturn(false);
        $this->pdo->method('query')->willReturn($this->statement);

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->expectException(Exception::class);

        $paginatedQuery->getPages();
    }


    public function testGetItemsQueryFails()
    {
        global $dbName;
        $this->statement->method('fetch')->willReturn(10); // Supposons qu'il y ait 10 éléments au total
        $this->pdo->method('query')->willReturn(false); // La requête échoue

        $paginatedQuery = new PaginatedQuery(
            "SELECT * FROM `{$dbName}`.files",
            "SELECT COUNT(id) FROM `{$dbName}`.files",
            $this->pdo,
            $this->ElementsPerPage
        );

        $this->expectException(Error::class);

        $paginatedQuery->getItems();

    }

    // Nettoyer les variables globales après chaque test
    protected function tearDown(): void
    {
        unset($_GET);
    }


}




