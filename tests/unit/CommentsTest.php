<?php

require_once getenv('PROJECT_ROOT').'src/inc/Plugins/Comments.php';

use PHPUnit\Framework\TestCase;
use Plugins\Comments;

/**
 * @coversDefaultClass \Plugins\Comments
 * Plan de test :
 * 1. Tester la suppression d'un commentaire existant.
 * 2. Tester la suppression d'un commentaire qui n'existe pas.
 * 3. Tester la suppression d'un commentaire avec des réponses.
 * 4. Tester la récupération de commentaires sans réponses.
 * 5. Tester la récupération de commentaires avec réponses.
 * 6. Tester le tri des réponses.
 * 7. Tester le tri des réponses par dates
 * 8. Tester l'enregistrement d'un nouveau commentaire.
 * 9. Tester l'enregistrement d'un commentaire avec des erreurs (par exemple, contenu vide).
 * 10. Tester l'enregistrement d'une réponse à un commentaire existant.
 * 11. Tester l'enregistrement d'une réponse à un commentaire inexistant.
 */

class CommentsTest extends TestCase
{
    private PDO $pdo;
    private Comments $comments;

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        $this->pdo = $this->createMock(PDO::class);
        $this->comments = new Comments($this->pdo);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testDeleteExistingComment()
    {
        // Créer un mock pour PDOStatement
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([['id' => 1, 'replies' => []]]);

        // Simuler la méthode prepare de PDO pour retourner le mock de PDOStatement
        $this->pdo->method('prepare')->willReturn($statementMock);

        // Simuler la réponse de la méthode exec de PDO
        $this->pdo->method('exec')->willReturn(1);

        // Appeler la méthode delete et vérifier le résultat
        $result = $this->comments->delete(1, 'ref', 123);
        $this->assertEquals(1, $result);
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testDeleteNonExistingComment()
    {
        // Créer un mock pour PDOStatement
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([]);

        // Simuler la méthode prepare de PDO pour retourner le mock de PDOStatement
        $this->pdo->method('prepare')->willReturn($statementMock);

        // Appeler la méthode delete et vérifier le résultat
        $result = $this->comments->delete(1, 'ref', 123);
        $this->assertEquals(0, $result);
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testDeleteWithReplies()
    {
        // Créer un mock pour PDOStatement
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('execute')->willReturn(true);
        $statementMock->method('fetchAll')->willReturn([['id' => 1, 'replies' => [(object)['id' => 2]]]]);

        // Simuler la méthode prepare de PDO pour retourner le mock de PDOStatement
        $this->pdo->method('prepare')->willReturn($statementMock);

        // Simuler la réponse de la méthode exec de PDO
        $this->pdo->method('exec')->willReturn(2);

        // Appeler la méthode delete et vérifier le résultat
        $result = $this->comments->delete(1, 'ref', 123);
        $this->assertEquals(2, $result);
        $this->assertArrayHasKey('success', $_SESSION['flash']);
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testFindAllWithNoReplies()
    {
        $this->pdo->method('prepare')
            ->willReturn($this->createMock(PDOStatement::class));

        $result = $this->comments->findAll('ref', 123);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testFindAllWithReplies()
    {
        $statement = $this->createMock(PDOStatement::class);
        $statement->method('fetchAll')
            ->willReturn([
                (object)['id' => 1, 'parent_id' => 0, 'created' => '2021-01-01 10:00:00'],
                (object)['id' => 2, 'parent_id' => 1, 'created' => '2021-01-01 11:00:00']
            ]);

        $this->pdo->method('prepare')->willReturn($statement);

        $result = $this->comments->findAll('ref', 123);
        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertCount(1, $result[0]->replies);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testFindAllWithInvalidRef()
    {
        $statement = $this->createMock(PDOStatement::class);
        $statement->method('fetchAll')->willReturn([]);

        $this->pdo->method('prepare')->willReturn($statement);

        $result = $this->comments->findAll('invalid_ref', 123);
        $this->assertIsArray($result);
        $this->assertEmpty($result);
    }

    public function testSortReplies()
    {
        $comment1 = (object)['id' => 1, 'created' => '2021-01-01 10:00:00'];
        $comment2 = (object)['id' => 2, 'created' => '2021-01-01 11:00:00'];

        $result = $this->comments->sortReplies($comment1, $comment2);
        $this->assertEquals(-1, $result);

        $result = $this->comments->sortReplies($comment2, $comment1);
        $this->assertEquals(1, $result);
    }

    public function testSortRepliesWithSameTime()
    {
        $comment1 = (object)['id' => 1, 'created' => '2021-01-01 10:00:00'];
        $comment2 = (object)['id' => 2, 'created' => '2021-01-01 10:00:00'];

        $result = $this->comments->sortReplies($comment1, $comment2);
        $this->assertEquals(1, $result);
    }

    public function testSaveWithEmptyContent()
    {
        $_POST['content'] = '';
        $result = $this->comments->save('ref', 123);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('content', $result);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testSaveWithValidData()
    {
        $_POST['content'] = 'Test comment';
        $_SESSION['auth'] = (object)['username' => 'user', 'email' => 'user@example.com'];

        $statement = $this->createMock(PDOStatement::class);
        $statement->method('execute')->willReturn(true);

        $this->pdo->method('prepare')->willReturn($statement);

        $result = $this->comments->save('ref', 123);
        $this->assertTrue($result);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testSaveWithInvalidParentId()
    {
        $_POST['content'] = 'Test comment';
        $_POST['parent_id'] = 999; // Non-existing parent ID
        $_SESSION['auth'] = (object)['username' => 'user', 'email' => 'user@example.com'];

        $statement = $this->createMock(PDOStatement::class);
        $statement->method('rowCount')->willReturn(0);

        $this->pdo->method('prepare')->willReturn($statement);

        $result = $this->comments->save('ref', 123);
        $this->assertIsArray($result);
        $this->assertArrayHasKey('parent_id', $result);
    }

    public function testSaveWithoutAuth()
    {
        $_POST['content'] = 'Test comment';
        unset($_SESSION['auth']);

        $result = $this->comments->save('ref', 123);
        $this->assertIsArray($result);
    }

    // Nettoyer les variables globales après chaque test
    protected function tearDown(): void
    {
        unset($_POST['content'], $_POST['parent_id'], $_SESSION['auth']);
    }

}
