<?php

require_once getenv('PROJECT_ROOT') . 'src/inc/Plugins/Vote.php';

use PHPUnit\Framework\TestCase;
use Plugins\Vote;

/**
 * @covers Vote
 * 1. TestLikeValidVote : Tester la fonction like avec un vote valide.
 * 2. TestLikeInvalidVote : Tester la fonction like avec un vote invalide.
 * 3. TestDislikeValidVote : Tester la fonction dislike avec un vote valide.
 * 4. TestDislikeInvalidVote : Tester la fonction dislike avec un vote invalide.
 * 5. TestVoteFunctionality : Tester la logique interne de la fonction vote.
 * 6. TestUpdateCount : Tester la mise à jour des compteurs de votes avec la fonction updateCount.
 * 7. TestSameVoteExists : Tester la suppression d'un vote existant.
 * 8. TestSameVoteDoesntExist : Tester l'ajout d'un vote inexistant.
 * 9. TestVoteWithException : Tester la fonction vote avec une exception.
 */

class VoteTest extends TestCase
{
    private PDO $pdo;
    private Vote $vote;

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        // Créer un mock pour PDO
        $this->pdo = $this->createMock(PDO::class);

        // Initialiser l'objet Vote
        $this->vote = new Vote($this->pdo);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testLikeValidVote()
    {
        // Simuler la réponse de la méthode prepare pour recordExists
        $statementRecordExists = $this->createMock(PDOStatement::class);
        $statementRecordExists->method('execute')->willReturn(true);
        $statementRecordExists->method('rowCount')->willReturn(1); // Simuler l'existence de l'enregistrement
        $this->pdo->method('prepare')->willReturn($statementRecordExists);

        // Simuler la réponse de la méthode query
        $statementQuery = $this->createMock(PDOStatement::class);
        $this->pdo->method('query')->willReturn($statementQuery);

        // Appeler la méthode like et vérifier le résultat
        $result = $this->vote->like('post', 1, 123);
        $this->assertTrue($result);
    }


    public function testLikeInvalidVote()
    {
        // Simuler une exception pour la méthode recordExists
        $this->pdo->method('prepare')->will($this->throwException(new Exception()));

        // Appeler la méthode like et vérifier le résultat
        $result = $this->vote->like('post', 1, 123);
        $this->assertFalse($result);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     * @throws Exception
     */
    public function testDislikeValidVote()
    {
        // Simuler la réponse de la méthode prepare pour recordExists
        $statementRecordExists = $this->createMock(PDOStatement::class);
        $statementRecordExists->method('execute')->willReturn(true);
        $statementRecordExists->method('rowCount')->willReturn(1); // Simuler l'existence de l'enregistrement
        $this->pdo->method('prepare')->willReturn($statementRecordExists);

        // Simuler la réponse de la méthode query
        $statementQuery = $this->createMock(PDOStatement::class);
        $this->pdo->method('query')->willReturn($statementQuery);

        // Appeler la méthode dislike et vérifier le résultat
        $result = $this->vote->dislike('post', 1, 123);
        $this->assertTrue($result);
    }


    /**
     * @throws Exception|\PHPUnit\Framework\MockObject\Exception
     */
    public function testDislikeInvalidVote()
    {
        $this->expectException(Exception::class);
        // Simuler une exception pour la méthode recordExists
        $this->pdo->method('prepare')->will($this->throwException(new Exception()));

        $statement = $this->createMock(PDOStatement::class);
        $statement->method('execute')->willReturn(true);
        $this->pdo->method('query')->willReturn($statement);

        // Appeler la méthode dislike et vérifier le résultat
        $result = $this->vote->dislike('post', 1, 123);
        $this->assertFalse($result);
    }

    /**
     * @throws ReflectionException
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testVoteMethod()
    {
        // Simuler la réponse de la méthode prepare pour recordExists
        $statementRecordExists = $this->createMock(PDOStatement::class);
        $statementRecordExists->method('execute')->willReturn(true);
        $statementRecordExists->method('rowCount')->willReturn(1); // Simuler l'existence de l'enregistrement
        $this->pdo->method('prepare')->willReturn($statementRecordExists);

        // Simuler la réponse de la méthode prepare pour les autres appels
        $statementOther = $this->createMock(PDOStatement::class);
        $statementOther->method('execute')->willReturn(true);
        $this->pdo->method('prepare')->willReturn($statementOther);

        // Utiliser la réflexion pour tester une méthode privée
        $method = new ReflectionMethod(Vote::class, 'vote');

        // Appeler la méthode vote et vérifier le résultat
        $result = $method->invokeArgs($this->vote, ['post', 1, 123, 1]);
        $this->assertTrue($result);
    }


    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testUpdateCount()
    {
        // Simuler la réponse de la méthode prepare et execute
        $statement = $this->createMock(PDOStatement::class);
        $statement->method('execute')->willReturn(true);
        $statement->method('fetchAll')->willReturn([['vote' => 1, 'count' => 5], ['vote' => -1, 'count' => 3]]);
        $this->pdo->method('prepare')->willReturn($statement);

        // Appeler la méthode updateCount et vérifier le résultat
        $result = $this->vote->updateCount('post', 1);
        $this->assertTrue($result);
    }

    public function testGetClass()
    {
        // Tester avec un vote positif
        $vote = (object)['vote' => 1];
        $result = Vote::getClass($vote);
        $this->assertEquals('is-liked', $result);

        // Tester avec un vote négatif
        $vote = (object)['vote' => -1];
        $result = Vote::getClass($vote);
        $this->assertEquals('is-disliked', $result);

        // Tester sans vote
        $result = Vote::getClass(null);
        $this->assertNull($result);
    }

    /**
     * @throws ReflectionException
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testVoteSameVoteExists()
    {
        // Simuler la réponse de la méthode prepare pour recordExists
        $statementRecordExists = $this->createMock(PDOStatement::class);
        $statementRecordExists->method('execute')->willReturn(true);
        $statementRecordExists->method('rowCount')->willReturn(1); // Simuler l'existence de l'enregistrement

        // Simuler la réponse de la méthode prepare pour le vote
        $statementVote = $this->createMock(PDOStatement::class);
        $statementVote->method('execute')->willReturn(true);
        $statementVote->method('fetch')->willReturn((object)['id' => 1, 'vote' => 1]);

        // Configurer les mocks pour les appels successifs à prepare
        $this->pdo->method('prepare')->willReturnOnConsecutiveCalls($statementRecordExists,
                                                                             $statementVote, $statementVote);

        // Appeler la méthode vote et vérifier que false est retourné
        $method = new ReflectionMethod(Vote::class, 'vote');
        $result = $method->invokeArgs($this->vote, ['post', 1, 123, 1]);
        $this->assertFalse($result);
    }

    /**
     * @throws ReflectionException
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testVoteDifferentVoteExists()
    {
        // Mock pour simuler un vote existant différent
        $statementRecordExists = $this->createMock(PDOStatement::class);
        $statementRecordExists->method('execute')->willReturn(true);
        $statementRecordExists->method('rowCount')->willReturn(1); // Simuler l'existence de l'enregistrement
        $statementRecordExists->method('fetch')->willReturn(['id' => 1, 'vote' => 0]);
        $this->pdo->method('prepare')->willReturn($statementRecordExists);

        // Appeler la méthode vote et vérifier que true est retourné
        $method = new ReflectionMethod(Vote::class, 'vote');
        $result = $method->invokeArgs($this->vote, ['post', 1, 123, 1]);
        $this->assertTrue($result);
    }

    /**
     * @throws ReflectionException
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testVoteNoVoteExists()
    {
        // Mock pour simuler qu'aucun vote n'existe
        $statementRecordExists = $this->createMock(PDOStatement::class);
        $statementRecordExists->method('execute')->willReturn(true);
        $statementRecordExists->method('rowCount')->willReturn(1); // Simuler l'existence de l'enregistrement
        $statementRecordExists->method('fetch')->willReturn(false);
        $this->pdo->method('prepare')->willReturn($statementRecordExists);

        // Appeler la méthode vote et vérifier que true est retourné
        $method = new ReflectionMethod(Vote::class, 'vote');
        $result = $method->invokeArgs($this->vote, ['post', 1, 123, 1]);
        $this->assertTrue($result);
    }

    /**
     * @throws ReflectionException
     */
    public function testVoteWithException()
    {
        // Configurer les mocks pour simuler une exception dans recordExists ou ailleurs
        $this->pdo->method('prepare')->will($this->throwException(new Exception("Database error")));

        // Appeler la méthode vote et vérifier qu'une exception est levée
        $this->expectException(Exception::class);
        $method = new ReflectionMethod(Vote::class, 'vote');
        $method->invokeArgs($this->vote, ['post', 1, 123, 1]);
    }

}
