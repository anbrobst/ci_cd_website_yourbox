<?php

require_once getenv('PROJECT_ROOT') . 'src/inc/Plugins/Posts.php';

use PHPUnit\Framework\TestCase;
use Plugins\Posts;

/**
 * @covers Posts
 * Plan de test :
 * 1. TestUpdateValidPost : Tester la mise à jour d'un post valide
 * 2. TestUpdateInvalidPost : Tester la mise à jour d'un post invalide
 */

class PostsTest extends TestCase
{
    private PDO $pdo;
    private Posts $posts;

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        // Créer un mock pour PDO
        $this->pdo = $this->createMock(PDO::class);

        // Initialiser l'objet Posts
        $this->posts = new Posts($this->pdo);
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     * @throws Exception
     */
    public function testUpdateValidPost()
    {
        // Créer un mock pour PDOStatement
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('execute')->willReturn(true);

        // Simuler la méthode prepare de PDO pour retourner le mock de PDOStatement
        $this->pdo->method('prepare')->willReturn($statementMock);

        // Créer un post mocké
        $post = new stdClass();
        $post->id = 1;
        $post->name = 'Updated Post Name';

        // Exécuter la méthode update et vérifier qu'aucune exception n'est levée
        $this->posts->update($post);
        $this->assertTrue(true); // Si aucune exception n'est levée, le test réussit
    }

    /**
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    public function testUpdateInvalidPost()
    {
        // Créer un mock pour PDOStatement
        $statementMock = $this->createMock(PDOStatement::class);
        $statementMock->method('execute')->willReturn(false);

        // Simuler la méthode prepare de PDO pour retourner le mock de PDOStatement
        $this->pdo->method('prepare')->willReturn($statementMock);

        // Créer un post mocké
        $post = new stdClass();
        $post->id = 1;
        $post->name = 'Invalid Post Name';

        // Exécuter la méthode update et s'attendre à une exception
        $this->expectException(Exception::class);
        $this->posts->update($post);
    }
}