#!/bin/bash

# empty variables
app_service_url=""
db_port=""
mail_pass=""
mail_user=""
mail_server=""

# Command-line arguments
sql_server_fqdn="$1"
sql_database_name="$2"
db_user="$3"
db_pass="$4"
project_root="$5"

ENV_FILE_PATH="$project_root.env.php"

# Create .env.php file
cat <<EOF > "$ENV_FILE_PATH"
<?php
putenv("PROJECT_ROOT=$project_root");
putenv("DB_PORT=$db_port");
putenv("DB_host=$sql_server_fqdn");
putenv("DB_name=$sql_database_name");
putenv("DB_user=$db_user");
putenv("DB_pass=$db_pass");
putenv("MAIL_pass=$mail_pass");
putenv("MAIL_host=$mail_server");
putenv("MAIL_user=$mail_user");
putenv("URL_root=$app_service_url");
EOF

echo $ENV_FILE_PATH +" file created successfully."
