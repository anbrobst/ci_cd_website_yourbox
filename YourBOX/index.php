<?php


global $pdo, $dbName;

require_once getenv('PROJECT_ROOT') . 'src/inc/Database/db.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/header.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/menu_nav.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/functions.php';

reconnect_from_cookie();

require_once getenv('PROJECT_ROOT') . 'src/inc/Plugins/PaginatedQuery.php';

use Plugins\PaginatedQuery;

$paginatedQuery = new PaginatedQuery("SELECT id FROM `{$dbName}`.files ORDER BY id DESC", "SELECT COUNT(id) FROM `{$dbName}`.files", $pdo, 15);
try {
    $answer = $paginatedQuery->getItems();
} catch (Exception $e) {
    echo $e->getMessage();
}
?>
<link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/styleHomePage.css'?>">
<div class="main">
    <?php if (isset($answer)): ?>

    <div class="video-list">
        <?php while ($id = $answer->fetch()): ?>
            <?php
            $id = $id -> id;
            $req = $pdo->query("SELECT * FROM `{$dbName}`.files WHERE id=$id");
            global $video_preview;
            global $thumbnail, $title_video;
            while($video_preview = $req->fetch())
            {
                $path_thumbnail = $video_preview -> URL_preview;
                $thumbnail = basename($path_thumbnail);
                $title_video = $video_preview->name;
                $file_path = $video_preview->file_url;
            ?>
            <section id="<?= $GLOBALS['video_preview']->id ?>" class="video_item_wrapper video_item_medium"
                     data-post_id="<?= $GLOBALS['video_preview']->id ?>" data-post_duration="<?= $GLOBALS['video_preview']->duration ?>">
                <a class="video_item_thumbnail js_show_loader" href="<?= getenv('URL_root') . 'src/videos/PHP/playing/video_page.php?id=' .$GLOBALS['video_preview']->id ?>" >
                    <div class="video_item_section video_item_title">
                        <header>
                            <h2 class="video_item_section_txt">
                                <?= $GLOBALS['title_video']?>
                            </h2>
                        </header>
                    </div>
                    <img
                            src="<?= getenv('URL_root') . 'cache/upload/' .$thumbnail ?>"
                            alt="Let's see" class="js_video_preview "
                            data-base_url="<?= getenv('URL_root') . 'cache/upload/' .$thumbnail ?>"
                            data-thumbs="[2..20]"
                            data-vid="<?= $file_path ?>"
                            data-post_url="<?= $file_path ?>"
                            data-srcset="<?= $file_path ?> 480w"
                            data-sizes="(max-width: 479px) 100vw, (min-width: 480px) and (max-width: 767px) 50vw, 33vw">
                    <div class="video_item_section video_item_stats clearfix">
                        <span class="pull-left"><?= $GLOBALS['video_preview']->duration ?></span>
                        <span class="pull-right"><i class="far fa-thumbs-up"></i><?= $GLOBALS['video_preview']->like_count ?></span>
                    </div>
                </a>
            </section>
        <?php } endwhile;  ?>
    </div>
    <div class="d-flex justify-content-between my-4 ml-auto" >
        <?php try {
            echo $paginatedQuery->previousLink(getenv('URL_root') . 'index.php');
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>
        <?php try {
            echo $paginatedQuery->nextLink(getenv('URL_root') . 'index.php');
        } catch (Exception $e) {
            echo $e->getMessage();
        } ?>
    </div>
    <?php endif; ?>
</div>


