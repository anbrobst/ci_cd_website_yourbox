<?php
/**
 * Page d'édition d'une vidéo.
 *
 * Cette page permet à l'administrateur d'éditer les informations d'une vidéo spécifique.
 * Elle utilise la classe PaginatedQuery pour récupérer les données de la vidéo,
 * la classe Posts pour gérer les opérations sur les vidéos,
 * et la classe Validator pour valider les données du formulaire.
 *
 * @global PDO      $pdo    L'objet de connexion de base de données PDO global.
 * @global string   $dbName Le nom de la base de données global.
 */

global $pdo, $dbName;

require_once getenv('PROJECT_ROOT').'src/Plugins/PaginatedQuery.php';
require_once getenv('PROJECT_ROOT').'src/Plugins/Posts.php';
require_once getenv('PROJECT_ROOT').'src/Plugins/Validator.php';

use Plugins\PaginatedQuery;
use Plugins\Posts;
use Plugins\Validator;

$id = $_GET['id'];
require_once getenv('PROJECT_ROOT'). 'src/inc/functions.php';
admin_restriction();
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';




$success = false;

$paginatedQuery = new PaginatedQuery("SELECT * FROM `{$dbName}`.files WHERE id = $id ", "SELECT COUNT(id) FROM `{$dbName}`.files WHERE id = $id", $pdo);

try {
    $post = $paginatedQuery->getItems()->fetch();
} catch (Exception $e) {
}

$postTable = new Posts($pdo);
$errors = [];
if(!empty($_POST)){
    Validator::lang('fr');
    $validator = new Validator($_POST);
    $validator->rule('required', 'name');
    $validator->rule('lengthBetween', 'name', 3, 50);
    $req = $pdo->prepare("SELECT id FROM `{$dbName}`.files WHERE `{$dbName}`.files.name = ?");
    $req -> execute([$_POST['name']]);
    $titre = $req->fetch();
    if($titre){
        $errors['name'][] = 'This title is associated with another video';
    }

    if (isset($post)){
        $post->name = $_POST['name'];
        if($validator->validate() and empty($errors)) {
            try {
                $postTable->update($post);
            } catch (Exception $e) {
            }
            $success = true;
        }else {
            $errors = $validator->errors();
        }
    }

}
?>

<?php if($success): ?>
<div class="alert alert-success">Vos changes ont bien été pris en compte</div>
<?php endif; ?>
<?php if(!empty($errors)): ?>
    <div class="alert alert-danger">Un problème est survenu, veuillez réessayer.</div>
<?php endif; ?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<h1> Editer la vidéo : <?php global $post; echo htmlentities($post->name) ?> </h1>
<form action="" method="post">
    <div class="form-group">
        <label for="name">Titre</label>
        <label>
            <input type="text" class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>" name="name" value="<?= htmlentities($post->name)?>">
        </label>
        <?php if(isset($errors['name'])):?>
            <div class="invalid-feedback">
                <?= implode('<br/>', $errors['name']) ?>
            </div>
        <?php endif; ?>
        
    </div>
    <button type="submit" class="btn btn-primary">Modifier</button>
</form>