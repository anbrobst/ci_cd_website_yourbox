<h1> Suppression de <?= $_GET['id']?></h1>
<?php
/**
 * Page de suppression d'un élément spécifique.
 *
 * Cette page affiche un titre indiquant la suppression de l'élément avec l'identifiant récupéré depuis la requête GET.
 * Elle gère la suppression de l'élément dans la base de données après vérifications d'autorisation.
 * En cas de succès, elle redirige vers la page d'index de l'administration avec un message de succès.
 * En cas d'échec, elle génère une exception indiquant l'impossibilité de supprimer l'élément.
 *
 * @global PDO      $pdo    L'objet de connexion de base de données PDO global.
 */
global $pdo;

if(empty($_POST)){
    $_SESSION['flash']['danger'] = "You are not authorized to access this page, it is dedicated to the administrator";
    header('Location: '.getenv('URL_root').'index.php');
    exit();
}
require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$id = $_GET['id'];
admin_restriction(); // TODO: make the restriction also apply to the person who posted it
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';


$req = $pdo->prepare('DELETE FROM files WHERE id= ?');

$ok = $req->execute([$id]);
if($ok === false){
    throw new Exception("It is impossible to remove this video:  $id");

}
$_SESSION['flash']['success'] = "The deletion was successful";
header('Location: '.getenv('URL_root').'src/admin/post/index.php');
exit();
?>


