<?php
/**
 * Page d'administration des vidéos.
 *
 * Cette page permet à l'administrateur de faire la gestion des vidéos, notamment d'afficher, éditer ou supprimer les vidéos existantes.
 * Elle utilise la classe PaginatedQuery pour récupérer les données des vidéos paginées.
 *
 * @global PDO      $pdo    L'objet de connexion de base de données PDO global.
 * @global string   $dbName Le nom de la base de données global.
 */
global $pdo, $dbName;

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
admin_restriction();
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
require_once getenv('PROJECT_ROOT').'src/inc/Plugins/PaginatedQuery.php';

use Plugins\PaginatedQuery;

$paginatedQuery = new PaginatedQuery("SELECT * FROM `{$dbName}`.files ORDER BY id DESC", "SELECT COUNT(id) FROM `{$dbName}`.files", $pdo);
try {
    $posts = $paginatedQuery->getItems()->fetchAll();
} catch (Exception $e) {
}
$link = getenv('URL_root').'src/accounts/admin/post/index.php';
require_once getenv('PROJECT_ROOT').'src/inc/header.php';
?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<table class="table">
    <thead>
        <th>#</th>
        <th>Titre</th>
        <th>Actions</th>
    </thead>
    <tbody>
    <tr>
        <?php global $posts; foreach ($posts as $post):?>
        <td>#<?= $post->id?></td>
        <td>
            <a href=<?= getenv('URL_root') . 'src/videos/PHP/playing/video_page.php?id=' . $post->id?>>
            <?= htmlentities($post->name)?>
            <a></a>
        </td>

        <td>
            <form style="display: inline" action="<?= getenv('URL_root').'src/accounts/admin/post/edit.php?id='.$post->id?>"  method="post" >
                <button type="submit" class="btn btn-primary">Edit</button>
                <form></form>
            <form style="display: inline" action="<?= getenv('URL_root').'src/accounts/admin/post/delete.php?id='.$post->id?>"  method="post" onsubmit=" return confirm('Etes vous sur de vouloir remove')" >
                <button type="submit" class="btn btn-danger">Remove</button>
            <form></form>

        </td>

    </tr>
    <?php endforeach;?>

    </tbody>

</table>
<div class="d-flex justify-content-between my-4 ml-auto">
    <?php try {
        echo $paginatedQuery->previousLink($link);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>
    <?php try {
        echo $paginatedQuery->nextLink($link);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>
</div>




<style>
    .btn-page:not(:disabled):not(.disabled){
        cursor: pointer;
    }


    .btn-page:hover {
        color: #fff;
        background-color: #bd2130;
        border-color: #bd2130;
        font-weight: bold;
    }

    .btn-page:focus, .btn-page:hover {
        text-decoration: none;
    }

    .btn-page {
        display: inline-block;
        text-align: center;
        white-space: nowrap;
        text-decoration: none;
        -webkit-writing-mode: horizontal-tb!important;
        border: 1px solid transparent;
        text-rendering: auto;
        letter-spacing: normal;
        word-spacing: normal;
        text-indent: 0;
        text-shadow: none;
        align-items: flex-start;
        font: 400 13px Arial;
        overflow: visible;
        margin: 0;
        font-family: inherit;
        vertical-align: middle;
        user-select: none;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

</style>