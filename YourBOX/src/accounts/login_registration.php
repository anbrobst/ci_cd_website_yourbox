<?php

/**
 * Page de connexion et d'inscription utilisateur.
 *
 * Cette page permet à un utilisateur de se connecter ou de s'inscrire.
 * Elle inclut également des fonctionnalités de rappel de mot de passe et de vérification de compte.
 * La page utilise un formulaire de connexion et un formulaire d'inscription, avec des champs pour le nom d'utilisateur, l'e-mail et le mot de passe.
 * La page inclut également des scripts pour basculer entre les formulaires de connexion et d'inscription.
 *
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 * @global string $dbName Le nom de la base de données global.
 */

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
reconnect_from_cookie();
if(isset($_SESSION['auth'])){
    header('Location: '.getenv('URL_root').'src/accounts/modification/modify_profile.php');
    exit();
}
require_once getenv('PROJECT_ROOT').'src/inc/header.php';
require_once getenv('PROJECT_ROOT'). 'src/inc/menu_nav.php' ;


?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>َLogin / Registration</title>
  </head>
  <link rel="stylesheet" href="../assets/stylesheets/registration_yourbox.css">
  <link href="https://cdn.discordapp.com/attachments/1075707754272018480/1165643292197798029/Logo.png?ex=654798e9&is=653523e9&hm=f927685ec96720760a359d6956a535958b1faf2415a6c0a8143f82d5ece17399&" rel="icon"/>
  <body>
  <div class="main">
  <script src="https://www.google.com/recaptcha/api.js"></script>
  <script>
      function onSubmit() {
          document.getElementById("box").submit();
      }
  </script>
<div style="margin-left: 28%">
<form action="login/login.php" autocomplete="on" id="box" method="POST">
  <h1>LOGIN</h1>
    <input autocomplete="on" required="required"  class="username" type="text" name="username" placeholder="Username or Email"/>
    <input autocomplete="on" required="required" type="password" name="password" placeholder="Password"/>
    <p class="keeplogin">
					<input type="checkbox" name="remember" id="loginkeeping" value="loginkeeping" />
					<label style="margin-left: 0px" for="loginkeeping">Stay logged in</label>
		</p>
    <a class="forget" href="modification/forget.php">Forgot your password ?</a>
    <input autocomplete="on" required="required" type="submit" name="" value="Login">
    <p class="change_link">
        Do not have an account yet ?
		<btn onclick="register()" class="to_register">Join yourBOX</btn>
    </p>
</form>

<form action="registration/registration.php" autocomplete="on" id="registration" method="POST">
  <h1>REGISTRATION</h1>
        <input autocomplete="on" required="required" type="text" name="username" placeholder="Username"/>
        <input autocomplete="on" required="required" type="text" name="email" placeholder="E-mail"/>
        <input autocomplete="on" required="required" type="password" name="password" placeholder="Password"/>
        <input autocomplete="on" required="required" type="password" name="confirmpassword" placeholder="confirm password"/>
    <fieldset>
        <legend>Your gender</legend>
        <div class="gender">
            <input type="radio" id="woman" name="gender" value="woman" >
            <label for="woman">I am a woman</label>

            <input type="radio" id="man" name="gender" value="man">
            <label for="man">I am a man</label>

            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
        </div>
    </fieldset>
  <p class="keeppassword">
        <input required="required" type="checkbox" name="passwordkeeping" id="passwordkeeping" value="passwordkeeping" />
        <label style="margin-left: 0px" for="passwordkeeping">I accept the terms and conditions.</label>
  </p>
  <input required="required" type="submit" name="registration" value="Register">
  <p class="change_link">
      Already have an account ?
  <btn onclick="login()" class="to_login">Log in to YourBOX</btn>
  </p>
</form>
<script>
    const x = document.getElementById("box");
    const y = document.getElementById("registration");

    function login(){
      x.style.display = "inherit";
      y.style.display = "none";
    }
    function register(){
      x.style.display = "none";
      y.style.display = "inherit";
    }
</script>
</div>
  </div>
  </body>
</html>