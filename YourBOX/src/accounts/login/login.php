<?php
/**
 * Script d'authentification utilisateur.
 *
 * Ce script gère l'authentification de l'utilisateur en fonction des données du formulaire soumises (nom d'utilisateur et mot de passe).
 *
 * @global PDO      $pdo    L'objet de connexion de base de données PDO global.
 * @global string   $dbName Le nom de la base de données global.
 */

global $pdo, $dbName;

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';


if(!empty($_POST) && !empty($_POST['username']) && !empty($_POST['password'])){
    require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
    $req = $pdo->prepare("SELECT * FROM `{$dbName}`.users WHERE (`{$dbName}`.users.username = :username OR `{$dbName}`.users.email = :username) AND `{$dbName}`.users.confirmed_at IS NOT NULL");
    $req->execute(['username' => $_POST['username']]);
    $user = $req->fetch();
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    if(isset($user->password))
    {
        if(password_verify($_POST['password'], $user -> password)){

            $_SESSION['auth'] = $user;
            $_SESSION['flash']['success'] = "You are now connected to YourBOX.";
            if(isset($_POST['remember'])){
                if($_POST['remember']) {
                    $remember_token = str_random(250);
                    $pdo->prepare("UPDATE `{$dbName}`.users SET `{$dbName}`.users.remember_token = ? WHERE id = ?")->execute([$remember_token, $user->id]);
                    setcookie('remember', $user->id . '==' . $remember_token . sha1($user->id . 'YourBox@$""56874563214'), time() + 60 * 60 * 24 * 7);
                }
            }
            header('Location: '.getenv('URL_root').'src/accounts/modification/modify_profile.php');
            exit();
        }
    }
    $_SESSION['flash']['danger'] = "The username or password is invalid";
    header('Location:  '.getenv('URL_root').'src/accounts/login_registration.php');
    exit();
}
?>
<!DOCTYPE html>
<html>
    <body>
        <p>Logged in</p>
    </body>
</html>
