<?php
/**
 * Script de déconnexion utilisateur.
 *
 * Ce script gère la déconnexion de l'utilisateur en supprimant le cookie de rappel,
 * en désinitialisant la session authentifiée et en redirigeant l'utilisateur vers la page d'accueil.
 */
session_start();
setcookie('remember', 0x00, -1);
unset($_SESSION['auth']);
$_SESSION['flash']['success'] = 'You are now disconnected';
header('Location: '.getenv('URL_root').'index.php');
exit();
?>
<!DOCTYPE html>
<html>
    <body>
        <p>Logged out</p>
    </body>
</html>
