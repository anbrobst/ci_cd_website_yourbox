<?php
/**
 * Page du profil utilisateur.
 *
 * Cette page affiche les informations du profil de l'utilisateur, permettant la modification de certaines informations
 * telles que la photo de profil, le nom d'utilisateur, le nombre de publications, d'abonnés et d'abonnements.
 *
 * @global string $profile_picture L'URL de la photo de profil de l'utilisateur.
 * @global object $data            Les données du profil de l'utilisateur.
 */

global $profile_picture, $data;
require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
page_restriction();



?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>My account</title>
    <link href="https://cdn.discordapp.com/attachments/1075707754272018480/1165643292197798029/Logo.png?ex=654798e9&is=653523e9&hm=f927685ec96720760a359d6956a535958b1faf2415a6c0a8143f82d5ece17399&" rel="icon">
    <link rel="stylesheet" href="<?=  getenv('URL_root').'src/assets/stylesheets/modify_profil.css' ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body>
<div class="main_page">
    <header class="infos">
        <div class="profile_picture">
            <div class="photo_display">
                <button class="change" title="Modifier la photo de profil">
                    <img class="photo" src="<?=$profile_picture?>" alt="Modifier la photo de profil">
                </button>
            </div>
        </div>
        <section class="info_principale">
            <div class="haut">
                <h2 class="_7UhW9       fKFbl yUEEX   KV-D4             fDxYl     "><?= $data -> username?></h2>
                <a class="modify_account" href="<?=  getenv('URL_root').'src/accounts/modification/modify_profile.php' ?>" tabindex="0"><button class="sqdOP  L3NKy _4pI4F   _8A5w5    " type="button">Modifier profil</button></a>
                <div class="modifier_info">
                    <button class="options" type="button"><i class="fas fa-sliders-h"></i></button>
                </div>
            </div>
            <ul class="milieu">
                <li class="publication"><span class="text"><span class="number">5</span>publications</span></li>
                <li class="abonnés"> <a class="text" href="/thonyan32/followers/" tabindex="0"><span class="number" title="123">123</span>abonnés</a></li>
                <li class="abonnement"> <a class="text" href="/thonyan32/following/" tabindex="0"><span class="number" title="230">230</span>abonnement</a></li>

            </ul>

        </section>
    </header>

    <main class="SCxLW  o64aR " role="main" style="height: 700px;width: 100%;">
        <div class="BvMHM EzUlV XfvCs">
            <ul class="wW1cu">
                <li><a class="h-aRd -HRM- " href="../modification/modify_profile.php" tabindex="0">Modifier profil</a></li>
                <li><a class="h-aRd  fuQUr" href="../modification/change_password.html" tabindex="0">Changer de mot de passe</a></li>
                <li><a  class="h-aRd  fuQUr" href="manage_access.php" tabindex="0">Apps et sites web</a></li>
                <li><a class="h-aRd  fuQUr" href="mail_text.php" tabindex="0">E-mail ou texto</a></li>
                <li><a class="h-aRd  fuQUr" href="management_file.php" tabindex="0">Mes fichiers</a></li>
                <li><a  class="h-aRd  fuQUr" href="security.php" tabindex="0">Sécurité et confidentialité</a></li>
            </ul>
        </div>
    </main>

</div>
<article class="PVkFi" style="position: absolute; top: 40%; left: 40%;">
    <form action="" class="kWXsT" method="POST" enctype="multipart/form-data">
    </form>
</article>

</body>
</html>