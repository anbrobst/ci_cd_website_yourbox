<?php
/**
 * Page de réinitialisation de mot de passe.
 *
 * Cette page permet à un utilisateur de réinitialiser son mot de passe en utilisant un lien unique généré par e-mail.
 * Elle vérifie la validité du lien, récupère l'utilisateur associé, et permet la modification du mot de passe.
 * Elle utilise également une redirection en cas de problème ou de succès.
 *
 * @global string $dbName Le nom de la base de données global.
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 */

global $dbName;
if(isset($_GET['id']) && isset($_GET['token'])){
    require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
    global $pdo;
    $req = $pdo->prepare("SELECT * FROM `{$dbName}`.users WHERE `{$dbName}`.users.id = ? AND `{$dbName}`.users.reset_token IS NOT NULL AND `{$dbName}`.users.reset_token = ? AND `{$dbName}`.users.reset_at > DATE_SUB(NOW(), INTERVAL 30 MINUTE)");
    $req->execute([$_GET['id'], $_GET['token']]);
    $user = $req -> fetch();
    if($user){
        if(!empty($_POST)){
            if(!empty($_POST['password']) && $_POST['password'] == $_POST['confirmpassword']){
                $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
                $pdo->prepare("UPDATE `{$dbName}`.users SET `{$dbName}`.users.password = ?, `{$dbName}`.users.reset_at = NULL, `{$dbName}`.users.reset_token = NULL WHERE id = ?") -> execute([$password, $_GET['id']]);
                if (session_status() === PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['flash']['success'] = 'Votre mot de passe a bien été modifé';
                $_SESSION['auth'] = $user;
                header('Location: modify_profile.php');
                exit();
            }
        }
    }else{
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $_SESSION['flash']['danger'] = "Un problème est survenu, veuillez réessayer";
        header('Location: '.getenv('URL_root').'index.php');
        exit();
    }
}else{
       header('Location: '.getenv('URL_root').'index.php');
       exit();
}


require_once getenv('PROJECT_ROOT').'src/inc/header.php';
?>




<link rel="stylesheet" href="../../assets/stylesheets/registration_yourbox.css">
<form style="position: relative;top:300px;" action="" autocomplete="on" id="box" method="POST">
    <h1>Réinitialisation de mon mot de passe</h1>
    <label>
        <input required="required" type="password" name="password" placeholder="Nouveau mot de passe"/>
    </label>
    <label>
        <input required="required" type="password" name="confirmpassword" placeholder="confirmation"/>
    </label>
    <input style ="padding: 10px; font-size: 11px" required="required" type="submit" name="" value="Réinitialiser votre mot de passe">
