<?php
    if (session_status() === PHP_SESSION_NONE) {
    session_start();
    }

global $dbName;
require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
global $pdo;



page_restriction();
ini_set('display_errors', 'off');
$id = $_SESSION['auth'] -> id;
$number_publication = $pdo->query("SELECT id FROM `{$dbName}`.files WHERE sender_id = $id")->rowCount();
$req = $pdo->query("SELECT username, email, bio, phone, profile_picture, name, gender FROM `{$dbName}`.users WHERE id = $id ");
global $data;
$data = $req->fetch();

$path_profile_picture = $data -> profile_picture;
global $profile_picture;
$profile_picture = basename($path_profile_picture);
?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>My account</title>
    <link href="https://cdn.discordapp.com/attachments/1075707754272018480/1165643292197798029/Logo.png?ex=654798e9&is=653523e9&hm=f927685ec96720760a359d6956a535958b1faf2415a6c0a8143f82d5ece17399&" rel="icon">
    <link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/modify_profil.css' ?>">
    <link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/registration_yourbox.css' ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body style="overflow-x:hidden">
<?php require_once getenv('PROJECT_ROOT').'src/inc/header.php'; ?>
<?php require_once getenv('PROJECT_ROOT').'src/inc/menu_nav.php' ?>
<div class="main" >
    <header class="infos">
        <div class="profile_picture">
            <div class="photo_display">
                <button class="change" title="Change profile picture">
                    <img height="150px" width="150px" class="photo" src="<?=getenv('URL_root').'cache/upload/'.$profile_picture?>" alt="Change profile picture">
                </button>
            </div>
        </div>
        <section class="main_info">
            <div class="haut">
                <h2 class="_7UhW9 fKFbl yUEEX KV-D4 fDxYl"><?= $data -> username?></h2>
                <a class="modify_account" href="modify_profile.php" tabindex="0"><button class="sqdOP  L3NKy _4pI4F   _8A5w5    " type="button">Edit profile</button></a>
                <div class="edit_info">
                    <button class="options" type="button" style="height: 38px;width: 46px;"><i class="fas fa-sliders-h"></i></button>
                </div>
            </div>
            <ul class="milieu">
                <li class="profile_number"><span class="text"><span class="number"><?= $number_publication?></span>publications</span></li>
                <li class="profile_number"> <a class="text" href="/<?= $data -> username?>/followers/" tabindex="0"><span class="number" title="number followers">123</span>followers</a></li>
                <li class="profile_number"> <a class="text" href="/<?= $data -> username?>/following/" tabindex="0"><span class="number" title="number following">230</span>following</a></li>

            </ul>

        </section>
    </header>

    <main class="SCxLW  o64aR " role="main" style="width: 100%;">
        <div class="BvMHM EzUlV XfvCs" style="display: flex; flex-direction: row;">
            <ul class="wW1cu">
                <li><a class="h-aRd  fuQur" href="modify_profile.php" tabindex="0">Edit profile</a></li>
                <li><a  class="h-aRd  -HRM-" href="change_password.php" tabindex="0">Change password</a></li>
                <li><a  class="h-aRd  fuQUr" href="../management/manage_access.php" tabindex="0">Apps and websites</a></li>
                <li><a  class="h-aRd  fuQUr" href="../management/mail_text.php" tabindex="0">Email or text</a></li>
                <li><a class="h-aRd  fuQUr" href="../management/management_file.php" tabindex="0">My files</a></li>
                <li><a  class="h-aRd  fuQUr" href="../management/security.php" tabindex="0" style="margin-bottom: 20px">Security and privacy</a></li>
            </ul>
          <article class="PVkFi">
              <form action="modify_password.php" class="kWXsT" autocomplete="on" id="box_mdp" method="POST" style="margin-top: auto; margin-bottom: auto; ">
                  <label>
                      <input required="required" type="password" name="password" placeholder="Nouveau mot de passe"/>
                  </label>
                  <label>
                      <input required="required" type="password" name="confirmpassword" placeholder="confirmation"/>
                  </label>
                  <input style ="padding: 10px;" required="required" type="submit" name="" value="Changer de mot de passe">
              </form>
          </article>
        </div>
        </main>

    </div>

      </body>
    </html>
