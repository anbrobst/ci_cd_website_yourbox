<?php

/**
 * Page de mise à jour du mot de passe utilisateur.
 *
 * Cette page permet à un utilisateur connecté de mettre à jour son mot de passe.
 * Elle vérifie si le formulaire a été soumis, si les champs de mot de passe sont non vides et si les mots de passe correspondent.
 * En cas de correspondance, elle met à jour le mot de passe dans la base de données et affiche un message de succès.
 *
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 * @global string $dbName Le nom de la base de données global.
 */

global $pdo, $dbName;
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
page_restriction();
if (!empty($_POST)){
    if(empty($_POST['password']) || $_POST['password'] != $_POST['confirmpassword']){
        $_SESSION['flash']['danger'] = "Les mots de passe ne correspondent pas";
    }else{
        $user_id = $_SESSION['auth']->id;
        $password= password_hash($_POST['password'], PASSWORD_BCRYPT);
        require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
        $pdo-> prepare("UPDATE `{$dbName}`.users SET `{$dbName}`.users.password = ? WHERE id = ?")-> execute([$password, $user_id]);
        $_SESSION['flash']['success'] = "Votre mot de passe a été mis à jour";

    }

}
require_once getenv('PROJECT_ROOT').'src/inc/header.php';

?>
