<?php global $pdo, $dbName;
/**
 * Page de récupération de mot de passe oublié.
 *
 * Cette page permet à un utilisateur d'initier le processus de récupération de mot de passe en fournissant son adresse e-mail.
 * Elle génère un jeton de réinitialisation, envoie un e-mail avec les instructions et redirige l'utilisateur.
 *
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 * @global string $dbName Le nom de la base de données global.
 */

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
if(!empty($_POST) && !empty($_POST['email']) ){
    require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
    require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
    $req = $pdo->prepare("SELECT * FROM `{$dbName}`.users WHERE `{$dbName}`.users.email = ? AND `{$dbName}`.users.confirmed_at IS NOT NULL");
    $req->execute([$_POST['email']]);
    $user = $req->fetch();
    if($user){
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
        $reset_token =str_random(60);
        $pdo->prepare("UPDATE `{$dbName}`.users SET `{$dbName}`.users.reset_token = ?, `{$dbName}`.users.reset_at = NOW() WHERE id = ?") -> execute([$reset_token, $user -> id]);

        $recipient = $_POST['email'];
        $subject = "Resetting your password on yourBOX";
        $body =  "To reset your password, please click on this link:
        \n\n ".getenv('URL_root')."src/accounts/registration/reset.php?id=$user&token=$reset_token";

        if (sendMail($recipient, $subject, $body)) {
            $_SESSION['flash']['success'] = "The instructions for changing your password have been sent to you by email";
            header('Location: '.getenv('URL_root').'index.php');
            exit();
        }
    }else{
        $_SESSION['flash']['danger'] = "The email does not appear to be associated with an account";
    }
}
require_once getenv('PROJECT_ROOT').'src/inc/header.php';
?>
<link rel="stylesheet" href="../../assets/stylesheets/registration_yourbox.css">

<form action="" autocomplete="on" id="box" method="POST">
    <h1>Forgot your password</h1>
    <label>
        <input required="required" type="email" name="email" placeholder="Please enter your email"/>
    </label>
    <input style ="padding: 10px;" required="required" type="submit" name="" value="Send">
