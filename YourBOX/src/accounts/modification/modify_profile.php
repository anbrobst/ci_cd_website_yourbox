<?php

/**
 * Page de profil utilisateur.
 *
 * Cette page affiche les informations du profil de l'utilisateur connecté, telles que le nom d'utilisateur, l'e-mail, la biographie,
 * le numéro de téléphone, l'image de profil, le nombre de publications, etc.
 * Elle utilise les fonctions de gestion des pages, se connecte à la base de données, et récupère les données du profil.
 *
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 * @global string $dbName Le nom de la base de données global.
 */

global $dbName;
require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
global $pdo;



page_restriction();
ini_set('display_errors', 'off');
$id = $_SESSION['auth'] -> id;
$number_publication = $pdo->query("SELECT id FROM `{$dbName}`.files WHERE sender_id = $id")->rowCount();
$req = $pdo->query("SELECT username, email, bio, phone, profile_picture, name, gender FROM `{$dbName}`.users WHERE id = $id ");
global $data;
$data = $req->fetch();

$path_profile_picture = $data -> profile_picture;
global $profile_picture;
$profile_picture = basename($path_profile_picture);

?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>My account</title>
    <link href="https://cdn.discordapp.com/attachments/1075707754272018480/1165643292197798029/Logo.png?ex=654798e9&is=653523e9&hm=f927685ec96720760a359d6956a535958b1faf2415a6c0a8143f82d5ece17399&" rel="icon">
    <link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/modify_profil.css' ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
<body style="overflow-x:hidden">
<?php require_once getenv('PROJECT_ROOT').'src/inc/header.php'; ?>
<?php require_once getenv('PROJECT_ROOT').'src/inc/menu_nav.php' ?>
<div class="main" >
    <header class="infos">
        <div class="profile_picture">
            <div class="photo_display">
                <button class="change" title="Change profile picture">
                    <img height="150px" width="150px" class="photo" src="<?=getenv('URL_root').'cache/upload/'.$profile_picture?>" alt="Change profile picture">
                </button>
            </div>
        </div>
        <section class="main_info">
            <div class="haut">
                <h2 class="_7UhW9 fKFbl yUEEX KV-D4 fDxYl"><?= $data -> username?></h2>
                <a class="modify_account" href="modify_profile.php" tabindex="0"><button class="sqdOP  L3NKy _4pI4F   _8A5w5    " type="button">Edit profile</button></a>
                <div class="edit_info">
                    <button class="options" type="button" style="height: 38px;width: 46px;"><i class="fas fa-sliders-h"></i></button>
                </div>
            </div>
            <ul class="milieu">
                <li class="profile_number"><span class="text"><span class="number"><?= $number_publication?></span>publications</span></li>
                <li class="profile_number"> <a class="text" href="/<?= $data -> username?>/followers/" tabindex="0"><span class="number" title="number followers">123</span>followers</a></li>
                <li class="profile_number"> <a class="text" href="/<?= $data -> username?>/following/" tabindex="0"><span class="number" title="number following">230</span>following</a></li>

            </ul>

        </section>
    </header>

    <main class="SCxLW  o64aR " role="main" style="width: 100%;">
        <div class="BvMHM EzUlV XfvCs" style="display: flex; flex-direction: row;">
            <ul class="wW1cu">
                <li><a class="h-aRd  -HRM-" href="modify_profile.php" tabindex="0">Edit profile</a></li>
                <li><a class="h-aRd  fuQUr" href="change_password.php" tabindex="0">Change password</a></li>
                <li><a class="h-aRd  fuQUr" href="../management/manage_access.php" tabindex="0">Apps and websites</a></li>
                <li><a class="h-aRd  fuQUr" href="../management/mail_text.php" tabindex="0">Email or text</a></li>
                <li><a class="h-aRd  fuQUr" href="../management/management_file.php" tabindex="0">My files</a></li>
                <li><a class="h-aRd  fuQUr" href="../management/security.php" tabindex="0">Security and privacy</a></li>
            </ul>
            <article class="PVkFi">
                <form action="modify_profile.php" class="kWXsT" method="POST" enctype="multipart/form-data">
                    <div class="eE-OA">
                        <aside class="sxIVS">Profile picture</aside>
                        <div class="ada5V">
                            <div class="AC7dP Igw0E IwRSH eGOV_ _4EzTm" style="width: 100%; max-width: 355px; margin-bottom: 25px;">
                                <input name="profile_picture" accept="image/jpeg,image/png" class="tb_sK" type="file" />
                            </div>
                        </div>
                    </div>
                    <div class="eE-OA">
                        <aside class="sxIVS"><label for="pepName">Name</label></aside>
                        <div class="ada5V">
                            <div class="AC7dP Igw0E IwRSH eGOV_ _4EzTm" style="width: 100%; max-width: 355px;">
                                <input name="name_profile" aria-required="false" id="pepName" type="text" class="JLJ-B zyHYP" value="<?= $data->name ?>" />
                                <div class="Igw0E IwRSH eGOV_ _4EzTm bkEs3 aGBdT" style="width: 100%; max-width: 355px;">
                                    <div class="Igw0E IwRSH eGOV_ _4EzTm MGdpg">
                                        <div class="_7UhW9 PIoXz MMzan _0PwGv uL8Hv"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="eE-OA">
                        <aside class="sxIVS"><label for="pepUsername">Username</label></aside>
                        <div class="ada5V">
                            <div class="AC7dP Igw0E IwRSH eGOV_ _4EzTm" style="width: 100%; max-width: 355px;"><input autocomplete="on" name="username" aria-required="true" id="pepUsername" type="text" class="JLJ-B zyHYP" value="<?= $data -> username?>" /></div>
                        </div>
                    </div>
                    <div class="eE-OA">
                        <aside class="sxIVS"><label for="pepBio">Bio</label></aside>
                        <div class="ada5V"><textarea name="bio"  class="p7vTm" id="pepBio"><?= $data -> bio?></textarea></div>
                    </div>
                    <div class="eE-OA">
                        <aside class="sxIVS tvweK"></aside>
                        <div class="ada5V">
                            <div class="Igw0E IwRSH eGOV_ _4EzTm aGBdT" style="width: 100%; max-width: 355px;">
                                <div class="Igw0E IwRSH eGOV_ _4EzTm pjcA_"><h2 class="JJF77">Personal information</h2></div>
                                <div class="_7UhW9 PIoXz MMzan _0PwGv uL8Hv">Provide your personal information. They will not be on your public profile.</div>
                            </div>
                        </div>
                    </div>
                    <div class="eE-OA">
                        <aside class="sxIVS"><label for="pepEmail">E-mail address</label></aside>
                        <div class="ada5V">
                            <div class="AC7dP Igw0E IwRSH eGOV_ _4EzTm" style="width: 100%; max-width: 355px;"><input autocomplete="on" name="email" aria-required="false" id="pepEmail" type="text" class="JLJ-B zyHYP" value="<?= $data -> email?>" /></div>
                        </div>
                    </div>
                    <div class="eE-OA">
                        <aside class="sxIVS"><label for="pepPhoneNumber">Phone number</label></aside>
                        <div class="ada5V">
                            <div class="AC7dP Igw0E IwRSH eGOV_ _4EzTm" style="width: 100%; max-width: 355px;"><input autocomplete="on" name="phone" aria-required="false" id="pepPhoneNumber" type="text" class="JLJ-B zyHYP" value="<?= $data -> phone?>" /></div>
                        </div>
                    </div>
                    <div class="Igw0E IwRSH eGOV_ _4EzTm">
                        <div class="eE-OA">
                            <aside class="sxIVS">Gender</aside>
                            <div class="ada5V">
                                <div class="Igw0E _56XdI eGOV_ vwCYk" style="max-width: 355px;">
                                    <button class="sqdOP yWX7d _4pI4F _8A5w5" type="button"><label>
                                            <input id="genderValue" readonly="" type="text" class="F0B8Y zyHYP" value="<?= $data -> gender?>" />
                                        </label></button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="eE-OA" style="left: 190px;">
                        <aside class="sxIVS tvweK"></aside>
                        <div class="ada5V">
                            <div class="fi8zo">
                                <input class="sqdOP L3NKy y3zKF"  style="margin-right: 15px;" value="Save" name="" type="submit">
                                <button class="M8zL6 sqdOP yWX7d y3zKF" type="button">Temporarily deactivate my account</button>
                            </div>
                        </div>
                    </div>
                </form>
            </article>
        </div>
    </main>
</div>

</body>
</html>




<?php
if (!empty($_POST )){
    $errors = array();
    require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';

    if(!preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])){
        $errors['username'] = "Your nickname is invalid";
    } else {
        $req = $pdo->prepare("SELECT id, username FROM `{$dbName}`.users WHERE `{$dbName}`.users.username = ?");
        $req -> execute([$_POST['username']]);
        $user = $req->fetch();
        if($user->username == $_POST['username'] && $user->id != $_SESSION['auth']->id){
            $errors['username'] = 'This username is already taken';
        }
    }

    if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Your email is invalid";
    } else {
        $req = $pdo->prepare("SELECT id FROM `{$dbName}`.users WHERE `{$dbName}`.users.email = ?");
        $req -> execute([$_POST['email']]);
        $user = $req->fetch();
        if($user){
            $errors['email'] = 'This email is associated with another account';
        }
    }

    if (preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#", $_POST['phone']))
    {
        $meta_characters = array("-", ".", " ");
        $_POST['phone'] = str_replace($meta_characters, "", $_POST['phone']);
        $_POST['phone'] = chunk_split($_POST['phone'], 2, "\r");
        echo $_POST['phone'];
    }
    else {
        $errors['phone'] = "Your phone is invalid";
    }

    $req = $pdo->prepare("SELECT id FROM `{$dbName}`.users WHERE `{$dbName}`.users.phone = ?");
    $req -> execute([$_POST['phone']]);
    $user = $req->fetch();
    if($user){
       $errors['phone'] = 'This phone is associated with another account';
    }
}


$name_profile_picture = basename($_FILES['profile_picture']['name']);
$folder_upload = getenv('PROJECT_ROOT'). 'cache/upload/';
$dest_profile_picture = $folder_upload . $name_profile_picture;

$size_maxi = 500000000;
$size = filesize($_FILES['profile_picture']['tmp_name']);
$extensions = array('.png', '.jpeg', '.jpg', );
$extension = strrchr($_FILES['profile_picture']['name'], '.');


if (!empty($_POST)) {
    $errors = array();
    $req = $pdo->prepare("SELECT id FROM `{$dbName}`.users WHERE `{$dbName}`.users.profile_picture = ?");
    $req->execute([$name_profile_picture]);
    $file_taken = $req->fetch();
    if ($file_taken) {
        $errors['taken_files'] = 'This file is already on the server';
    }
    if (empty($errors)) {


        if (!empty($_FILES)) {
            if (!in_array($extension, $extensions)) {
                $_SESSION['flash']['danger'] = "Wrong file format, those allowed are: jpeg and png";
                exit();

            }
            if ($size > $size_maxi) {
                $error = "The file is too big, 2Gb maximum";
                exit();
            }
            if (!isset($error)) {
                //We format the file name here...
                $name_profile_picture = strtr($name_profile_picture,
                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                $name_profile_picture = preg_replace('/([^.a-z0-9]+)/i', '-', $name_profile_picture);
                $name_profile_picture = strtr($name_profile_picture,
                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                $name_profile_picture = preg_replace('/([^.a-z0-9]+)/i', '-', $name_profile_picture);

                move_uploaded_file($_FILES['profile_picture']['tmp_name'], $dest_profile_picture);

            }
        }


        if (empty($errors)) {
            $req = $pdo->prepare("UPDATE `{$dbName}`.users SET `{$dbName}`.users.username = ?, `{$dbName}`.users.phone = ?,
                         `{$dbName}`.users.email = ?, `{$dbName}`.users.bio = ?, `{$dbName}`.users.name= ?, `{$dbName}`.users.profile_picture= ?, 
                         `{$dbName}`.users.confirmation_token= ? WHERE id = ?");
            $token = str_random(60);
            $req->execute([$_POST['username'], $_POST['phone'], $_POST['email'], $_POST['bio'], $_POST['name_profile'],
                $dest_profile_picture, $token, $_SESSION['auth']->id]);
            if ($_POST['email'] != $data->email) {

                $user_id = $_SESSION['auth']->id;
                $recipient = $_POST['email'];
                $subject = "Confirming your email";
                $body =  "To confirm your new email please click on this link: 
                    \n\n  ".getenv('URL_root')."src/accounts/registration/confirm.php?id=$user_id&token=$token";

                if (sendMail($recipient, $subject, $body)) {
                    $_SESSION['flash']['success'] = 'You will receive a confirmation email in a few moments, 
                    remember to check your folder_upload SPAM.';
                    header('Location: '.getenv('URL_root').'src/accounts/modification/modify_profile.php');
                    exit();
                }
            }
            $_SESSION['flash']['success'] = 'Your profile has been updated';
            header('Location: '.getenv('URL_root').'src/accounts/modification/modify_profile.php');
            exit();
        }

    }
}
?>









<?php if(!empty($errors)): ?>
    <div class="alert alert-danger">
        <p>You have not completed the form correctly</p>
        <ul>
            <?php foreach($errors as $error): ?>
                <li><?= $error; ?></li>
            <?php endforeach; ?>

        </ul>

    </div>
<?php endif; ?>