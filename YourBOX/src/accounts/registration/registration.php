<?php
/**
 * Page d'enregistrement d'un nouvel utilisateur.
 *
 * Cette page permet à un utilisateur de s'inscrire en fournissant un nom d'utilisateur, une adresse e-mail et un mot de passe.
 * Elle vérifie la validité des données fournies, envoie un e-mail de confirmation avec un lien unique, et redirige l'utilisateur en cas de succès.
 *
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 * @global string $dbName Le nom de la base de données global.
 */

global $pdo, $dbName;

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if (!empty($_POST )){
    $errors = array();
    require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';

    if(empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])){
        $errors['username'] = "Your nickname is invalid";
    } else {
        $req = $pdo->prepare("SELECT id FROM `{$dbName}`.users WHERE `{$dbName}`.users.username = ?");
        $req -> execute([$_POST['username']]);
        $user = $req->fetch();
        if($user){
            $errors['username'] = 'This username is already taken';
        }
    }

    if(empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Your email is invalid";
    } else {
        $req = $pdo->prepare("SELECT id FROM `{$dbName}`.users WHERE `{$dbName}`.users.email = ?");
        $req -> execute([$_POST['email']]);
        $user = $req->fetch();
        if($user){
            $errors['email'] = 'This email is associated with another account';
        }
    }

    if(empty($_POST['password']) || $_POST['password'] != $_POST['confirmpassword'] ){
        $errors['email'] = "You have not entered a password or the 2 passwords do not match";
    }


if(empty($errors)) {
    $req = $pdo->prepare("INSERT INTO `{$dbName}`.users SET `{$dbName}`.users.username = ?, `{$dbName}`.users.password = ?, `{$dbName}`.users.email = ?,`{$dbName}`.users.gender = ?, `{$dbName}`.users.confirmation_token = ?");
    $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $token = str_random(60);
    $req->execute([$_POST['username'], $password, $_POST['email'], $_POST['gender'], $token]);
    $user_id = $pdo->lastInsertId();
    $recipient = $_POST['email'];
    $subject = "Confirming your account";
    $body =  "To confirm your account, please click on this link:
     \n\n  ".getenv('URL_root')."src/accounts/registration/confirm.php?id=$user_id&token=$token";


    if (sendMail($recipient, $subject, $body)) {
        $_SESSION['flash']['success'] = "You will receive a confirmation email in a few moments, remember to check your folder SPAM.";
        header('Location: '.getenv('URL_root').'index.php');
        exit();
    }


}

}

?>

<?php if(!empty($errors)): ?>
<div class="alert alert-danger">
    <p>You have not completed the form correctly</p>
    <ul>
        <?php foreach($errors as $error): ?>
            <li><?= $error; ?></li>
        <?php endforeach; ?>

    </ul>

</div>
<?php endif; ?>
