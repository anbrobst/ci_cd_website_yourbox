<?php
/**
 * Page de validation de compte utilisateur.
 *
 * Cette page permet de valider le compte utilisateur en utilisant un lien unique généré par e-mail.
 * Elle vérifie la validité du lien en comparant l'identifiant utilisateur et le jeton de confirmation,
 * met à jour la confirmation du compte dans la base de données, et redirige l'utilisateur en cas de succès ou d'échec.
 *
 * @global PDO    $pdo    L'objet de connexion de base de données PDO global.
 * @global string $dbName Le nom de la base de données global.
 */
global $pdo, $dbName;

$user_id = $_GET['id'];
$token = $_GET['token'];
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
$req = $pdo->prepare("SELECT * FROM `{$dbName}`.users WHERE `{$dbName}`.users.id = ?");
$req -> execute([$user_id]);
$user = $req->fetch();
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if($user && $user->confirmation_token == $token ){
    $req = $pdo->prepare("UPDATE `{$dbName}`.users SET `{$dbName}`.users.confirmation_token =NULL , `{$dbName}`.users.confirmed_at = NOW() WHERE `{$dbName}`.users.id = ?")->execute([$user_id]);
    $_SESSION['flash']['success'] = 'Your account has been successfully validated';
    $_SESSION['auth'] = $user;
    header('Location: ../modification/modify_profile.php');
    exit();
} else{
    $_SESSION['flash']['danger'] = "This session is no longer valid, please try registration again";
    header('Location: registration.php');
    exit();
}

?>