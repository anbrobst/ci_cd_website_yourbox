<?php

require getenv('PROJECT_ROOT').'vendor/autoload.php';

use phpseclib3\Net\SSH2;
use phpseclib3\Crypt\PublicKeyLoader;



$dbPort = getenv('DB_PORT'); # 3306 by default
$dbName = getenv('DB_name');
$dbUser = getenv('DB_user');
$dbPass = getenv('DB_pass');
$sshUser = getenv('SSH_USER');
$sshHost = getenv('DB_host');
$sshPort = getenv('SSH_PORT'); # 22 by default
$sshPrivateKey = getenv('SSH_Private_KEY');
$sshPublicKey = getenv('SSH_Public_KEY');




try {
    $pdo = new PDO("mysql:port=$dbPort;dbname=$dbName;host=$sshHost",
                    $dbUser,
                    $dbPass,
                    [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
    $pdo -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

