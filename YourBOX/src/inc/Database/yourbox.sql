-- MySQL dump 10.13  Distrib 8.0.35, for Linux (x86_64)
--
-- Host: 46.105.31.41    Database: YourBOX
-- ------------------------------------------------------
-- Server version	8.0.35-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created` datetime NOT NULL,
  `ref` varchar(60) NOT NULL,
  `ref_id` int NOT NULL,
  `parent_id` int NOT NULL,
  `like_count` int NOT NULL DEFAULT '0',
  `dislike_count` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=700 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'john doe','john-doe@doe.fr','Bonjour, je suis un très bon commentaire.','2020-07-16 15:25:14','files',5,0,0,0),(689,'thonyan32','anthonybrobst760@gmail.com','Bonjour j\'aime beaucoup ce site. Il est super !!!!!','2020-07-21 21:13:02','files',5,0,0,0),(690,'thonyan32','anthonybrobst760@gmail.com','Je suis  d\'accord avec toi.','2020-07-21 21:13:26','files',5,689,0,0),(691,'thonyan32','anthonybrobst760@gmail.com','J\'en ai marre.','2020-07-22 16:11:32','files',5,0,0,0),(697,'thonyan32','anthonybrobst760@gmail.com','I don\'t know what to reply','2023-10-15 20:09:41','files',5,689,0,0),(699,'bloonie','benjamin63@free.fr','Trop bien le CSS de ce site\r\n','2023-11-20 10:29:52','files',5,0,0,0);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `files`
--

DROP TABLE IF EXISTS `files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `files` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `size` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `file_url` varchar(255) NOT NULL,
  `date_upload` varchar(60) DEFAULT NULL,
  `URL_preview` varchar(250) NOT NULL,
  `resolution` varchar(60) DEFAULT NULL,
  `keywords` varchar(512) DEFAULT NULL,
  `number_of_views` bigint DEFAULT NULL,
  `like_count` int DEFAULT '0',
  `dislike_count` int DEFAULT '0',
  `sender_id` int DEFAULT NULL,
  `description` text,
  `main_category` text,
  `creator` text,
  `complete_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `files`
--

LOCK TABLES `files` WRITE;
/*!40000 ALTER TABLE `files` DISABLE KEYS */;
INSERT INTO `files` VALUES (5,'OnePiece-WeAre-Song','30471243','04:02','C:/Users/a_bro/OneDrive - Université Clermont Auvergne/ISIMA/ZZapp2/1_Cours/Semestre 1/Intégration Continue/Projet/ci_cd_website_yourbox/YourBOX/cache/upload/one-piece-amv-we-are-full-song.mp4','2023-10-09 21:32:58','C:/Users/a_bro/OneDrive - Université Clermont Auvergne/ISIMA/ZZapp2/1_Cours/Semestre 1/Intégration Continue/Projet/ci_cd_website_yourbox/YourBOX/cache/upload/One-Piece-2-1-640x360.jpg','960x720\n',NULL,280,1,0,3,'First opening of One Piece #TooGood','Music','Toei Animation','one-piece-amv-we-are-full-song.mp4'),(6,'End-Of-Everything','29501919','07:13','C:/Users/a_bro/OneDrive - Université Clermont Auvergne/ISIMA/ZZapp2/1_Cours/Semestre 1/Intégration Continue/Projet/ci_cd_website_yourbox/YourBOX/cache/upload/end-of-everything-handpanepic.mp4','2023-10-14 09:14:51','C:/Users/a_bro/OneDrive - Université Clermont Auvergne/ISIMA/ZZapp2/1_Cours/Semestre 1/Intégration Continue/Projet/ci_cd_website_yourbox/YourBOX/cache/upload/End_of_Everything.jpg','1920x1080\n',NULL,44,0,0,3,'Very great music with hand pan                        ','Music','Didi Chandouidoui','end-of-everything-handpanepic.mp4');
/*!40000 ALTER TABLE `files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `confirmation_token` varchar(60) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `reset_token` varchar(60) DEFAULT NULL,
  `reset_at` datetime DEFAULT NULL,
  `remember_token` varchar(250) DEFAULT NULL,
  `bio` varchar(1000) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `gender` varchar(60) DEFAULT NULL,
  `admin` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'thonyan32','anthonybrobst760@gmail.com','$2y$10$Y0Hl2lnEjVh.VZ2ikuREOukLZuWlpoIA0NT8fAWtPmj3K6uAFD4Qy','bWZmZFV38uOtN6wGB5MoEOHIwZl6shfFqBF9ilm6STaKx8ppXfCxd7YRYGjM','2023-10-01 16:31:23',NULL,NULL,'17BXKaVqtHmALjr0k9miNfgxkFfenQwtNnWcp2vRljcdU1bpnQFHw7dY1QY6K5vG6AyZCuoH5bD7XIqe2GD83XTDzZZEdodFru2pHmSNIqYD3zcsrIoiufDevyzyGRmPHS4aeuyNN4p1ovVwYva5wUFCOsZB1znNxbhSoo6HY9Xks0DV0d54zPQHShpjUU8NbF4bh1nQbozFBx1U9PlkrMf2iqBA86QRhKzfcApReHaDi4AgJjjWX0nlsq','','','C:/Users/a_bro/OneDrive - Université Clermont Auvergne/ISIMA/ZZapp2/1_Cours/Semestre 1/Intégration Continue/Projet/ci_cd_website_yourbox/YourBOX/cache/upload/tardis-doctor-who_5f2bd5410052f.jpg','','man',NULL),(7,'a_bro','a_brobst@yahoo.fr','$2y$10$homOjpEeVgoAXG9ZEHqSAuozPo4zj1h9kTP5aKn8xuXAQRUCT0P/S',NULL,'2023-10-21 17:45:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'man',NULL),(8,'nounou','yufjghjh@jkhdhj.fr','$2y$10$6esnI8kJIBLq.NnjqU1qnuWSTxbH0qu8T0TG/ZeZNSyRaET.GBW4.','812lARVeLACFEwNVLbnn6BCS3iKrCBDTBgWd39bzevIKuxCxEtcZ4BsQBf6I',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'woman',NULL),(9,'nini','kjhhkkh@gh.fr','$2y$10$n./WQKZoptzYUflPxYzQKeQNnTBMrzYMHIa29CFKMJoIJlQYOqM2m','7nCVKdLZqAaZCDNtCXjeqf6k8rWfyrrczpOeAsOHZSBocw1xszSEhDQEXdbB',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'woman',NULL),(10,'bloonie','benjamin63@free.fr','$2y$10$wCpPl4keAsIYJNZXJsk0Iey5JTU3R4lLEQHdR8HACjVgfjXxUDVAC',NULL,'2023-11-03 09:21:15',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'other',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `votes`
--

DROP TABLE IF EXISTS `votes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `votes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ref_id` int DEFAULT NULL,
  `ref` varchar(50) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `vote` int DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_id` (`ref_id`,`ref`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `votes`
--

LOCK TABLES `votes` WRITE;
/*!40000 ALTER TABLE `votes` DISABLE KEYS */;
INSERT INTO `votes` VALUES (2,5,'files',10,1,'2023-11-20 13:42:32'),(3,16,'files',10,1,'2023-11-27 14:14:58');
/*!40000 ALTER TABLE `votes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-27 18:36:22
