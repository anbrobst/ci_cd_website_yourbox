<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require getenv('PROJECT_ROOT').'vendor/autoload.php';
/**
 * Affiche une variable de manière formatée dans une balise <pre>.
 *
 * @param mixed $variable La variable à afficher.
 * @return void
 */
function debug($variable): void
{
    echo '<pre>' .print_r($variable, true) . '</pre>';
}
/**
 * Génère une chaîne de caractères aléatoire.
 *
 * @param int $length La longueur de la chaîne générée.
 * @return string La chaîne de caractères aléatoire.
 */
function str_random($length): string
{
    $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
    return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
}
/**
 * Restreint l'accès à une page aux utilisateurs connectés.
 * Redirige vers la page d'accueil si l'utilisateur n'est pas authentifié.
 *
 * @return void
 */
function page_restriction(): void
{
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }

    if(!isset($_SESSION['auth'])){
        $_SESSION['flash']['danger'] = "You are not authorized to access this page";
        header('Location: '.getenv('URL_root').'index.php');
        exit();
    }
}
/**
 * Restreint l'accès à une page aux administrateurs.
 * Redirige vers la page d'accueil si l'utilisateur n'est pas administrateur.
 *
 * @return void
 */
function admin_restriction(): void
{
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }

    if(!isset($_SESSION['auth']) && $_SESSION['auth']->admin != 1){
        $_SESSION['flash']['danger'] = "You are not authorized to access this page, it is dedicated to the administrator";
        header('Location: '.getenv('URL_root').'index.php');
        exit();
    }
}
/**
 * Reconnecte automatiquement un utilisateur à partir d'un cookie de rappel (remember).
 * Si l'utilisateur possède un cookie de rappel valide, il est automatiquement connecté.
 *
 * @return void
 */
function reconnect_from_cookie(): void
{
    global $dbName;
    if(session_status() === PHP_SESSION_NONE){
        session_start();
    }
    if(isset($_COOKIE['remember']) && !isset($_SESSION['auth']) ){

        require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
        if(!isset($pdo)){
            global $pdo;
        }
        $remember_token = $_COOKIE['remember'];
        $parts = explode('==', $remember_token);
        $user_id = $parts[0];
        $req = $pdo->prepare("SELECT * FROM `{$dbName}`.users WHERE id = ?");
        $req->execute([$user_id]);
        $user = $req->fetch();
        if($user){
            $expected = $user_id . '==' . $user->remember_token . sha1($user_id . 'YourBOXbrfduzahrfuyiazqàrfazquigbrf56879/*)@&@');
            if($expected == $remember_token){
                if (session_status() === PHP_SESSION_NONE) {
                    session_start();
                }
                $_SESSION['auth'] = $user;
                $_SESSION['flash'] = null;
                setcookie('remember', $remember_token, time() + 60 * 60 * 24 * 7);
            } else{
                setcookie('remember', null, -1);
            }
        }else{
            setcookie('remember', null, -1);
        }
    }
}
/**
 * Obtient la durée et la résolution d'une vidéo spécifiée en utilisant ffprobe.
 *
 * @param string $video Le chemin de la vidéo.
 * @return array Les attributs de la vidéo (durée et résolution).
 */
function _get_video_attributes($video): array
{

    $duration_complete = shell_exec('ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 -sexagesimal'. " ". $video);
    $split_duration = explode('.' , $duration_complete);
    $duration = $split_duration[0];
    $resolution = shell_exec('ffprobe -v error -select_streams v:0 -show_entries stream=height,width -of csv=s=x:p=0'. " ". $video);


    return array('duration' => $duration,
            'resolution' => $resolution
    );
}
/**
 * Convertit une taille de fichier en octets en une chaîne lisible par l'homme.
 *
 * @param int $bytes La taille du fichier en octets.
 * @param int $decimals Le nombre de décimales à afficher.
 * @return string La taille formatée du fichier.
 */
function _human_filesize($bytes, $decimals = 2): string
{
    $sz = ['B', 'K', 'M', 'G', 'T', 'P'];
    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor < 0) $factor = 0; // To handle cases where $bytes is less than 10
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . $sz[$factor];
}
/**
 * Supprime les accents d'une chaîne de caractères.
 *
 * @param string $str La chaîne de caractères d'entrée.
 * @return array|string|null La chaîne sans accents.
 */
function removeAccents($str): array|string|null
{
    $replace = array(
        'Ç' => 'C', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'à' => 'a', 'á' => 'a',
        'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'À' => 'A', 'Á' => 'A',
        'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'ì' => 'i', 'í' => 'i',
        'î' => 'i', 'ï' => 'i', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'ð' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o',
        'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'ù' => 'u',
        'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U',
        'Ü' => 'U', 'ý' => 'y', 'ÿ' => 'y', 'Ý' => 'Y'
    );
    return strtr($str, $replace);
}
/**
 * Remplace certains caractères spéciaux dans une chaîne de caractères.
 *
 * @param string $string La chaîne de caractères d'entrée.
 * @return array|string La chaîne avec les caractères spéciaux remplacés.
 */
function removeSpecialChars($string): array|string
{
    $string = strtr($string,"' @\"\\/#,()","----------");
    return str_replace("--","-",$string);
}

/**
 * Envoie un courriel en utilisant la bibliothèque PHPMailer.
 *
 * @param mixed $recipient L'adresse e-mail du destinataire.
 * @param string $subject Le sujet du courriel.
 * @param string $body Le corps du courriel.
 * @return bool True si le courriel est envoyé avec succès, sinon False.
 */
function sendMail(mixed $recipient, string $subject, string $body): bool
{
    $mail = new PHPMailer(true);  // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                 // Print debugging information
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = getenv('MAIL_host');                 // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                             // Enable SMTP authentication
        $mail->Username = getenv('MAIL_user');
        $mail->Password = getenv('MAIL_pass');
        $mail->SMTPSecure = 'ssl';
        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->Port = 465;                              // SSL Port

        // Recipients
        $mail->setFrom($mail->Username, 'YourBOX');
        $mail->addAddress($recipient);                        // Add a recipient

        // Content
        $mail->isHTML();                                  // Define email format to HTML
        $mail->Subject = $subject;
        $mail->Body = $body;

        $mail->send();
        return True;

    } catch (Exception) {
        echo "Message could not be sent. Mailer Error: $mail->ErrorInfo";
        return false;
    }

}
