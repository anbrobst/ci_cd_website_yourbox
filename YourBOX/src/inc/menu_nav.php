<?php
if(session_status() === PHP_SESSION_NONE){
    session_start();
}

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
reconnect_from_cookie();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>

<body>
<div class="sidenav">
    <?php if(isset($_SESSION['flash'])): ?>
        <?php foreach($_SESSION['flash'] as $type => $message):?>
            <div class="alert" style="background-color: gold;padding: 15px;width: 237px; display: block; box-sizing: initial; margin-bottom: 8px" >
                <?= $message; ?>
            </div>
        <?php endforeach; ?>
        <?php unset($_SESSION['flash']); ?>
    <?php endif; ?>
    <a class="sidenav-solo" href="<?= getenv('URL_root') . 'index.php' ?>">Home</a>
    <div class="sidenav-line"> </div>

    <button class="sidenav-dropdown-button">
        Vidéos
        <i class="fa fa-caret-down"></i>
    </button>
    <div class="sidenav-dropdown-container">
        <a href=<?= getenv('URL_root') . 'src/videos/PHP/uploading/form_upload_video.php' ?>>Upload</a>
        <a href="#">Elements</a>
        <a href="#">Icons</a>
    </div>
    <div class="sidenav-line"> </div>

    <button class="sidenav-dropdown-button">
        Catégories
        <i class="fa fa-caret-down"></i>
    </button>
    <div class="sidenav-dropdown-container">
        <a href="#">Trends</a>
        <a href="#">Most views</a>
    </div>
    <div class="sidenav-line"> </div>

    <a class="sidenav-solo" href="#">Actors</a>
    <div class="sidenav-line"> </div>
    <a class="sidenav-solo" href="#">Community</a>
    <div class="sidenav-line"> </div>
    <a class="sidenav-solo" href="#">Pictures&Gifs</a>
    <div class="sidenav-line"> </div>
</div>
</body>
<script>
    //* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
    var dropdown = document.getElementsByClassName("sidenav-dropdown-button");
    var i;

    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            var dropdownContent = this.nextElementSibling;
            var arrow = this.children[0];
            if (dropdownContent.style.display === "initial") {
                dropdownContent.style.display = "none";
                arrow.style.rotate = "270deg";
            } else {
                dropdownContent.style.display = "initial";
                arrow.style.rotate = "0deg";
            }
        });
    }
</script>
</html>

