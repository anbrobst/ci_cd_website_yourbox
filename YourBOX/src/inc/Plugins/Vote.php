<?php


namespace Plugins;
use Exception;
use PDO;



require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';


class Vote{
    private PDO $pdo;

    private mixed $former_vote;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    /**
     * @throws Exception
     */
    private function recordExists($ref, $ref_id): void
        {
            global $dbName;
            $req = $this->pdo->prepare("SELECT * FROM `{$dbName}`.$ref WHERE `{$dbName}`.$ref.id = ?");
            $req->execute([$ref_id]);
            if($req->rowCount() == 0){
                throw new Exception("This function is currently unavailable because the recording does not exist.");
            }

        }
    /**
     * Méthode pour gérer le vote "J'aime"
     * @param string $ref
     * @param int $ref_id
     * @param int $user_id
     * @return bool
     * @throws Exception
     */
    public function like($ref, $ref_id, $user_id): bool
    {
        global $dbName;
        try {
            if ($this->vote($ref, $ref_id, $user_id, 1)) {
                $sql_part = "";
                if (isset($this->former_vote)) {
                    if ($this->former_vote) {
                        $sql_part = ", `{$dbName}`.".$ref.".dislike_count = 
                                        CASE 
                                        WHEN `{$dbName}`.$ref.dislike_count > 0 
                                        THEN `{$dbName}`.$ref.dislike_count - 1 
                                        ELSE 0 
                                        END";

                    }
                }
                $query = "UPDATE `{$dbName}`.".$ref.
                         " SET `{$dbName}`.".$ref.".like_count = `{$dbName}`.".$ref.".like_count + 1".
                         $sql_part.
                         " WHERE `{$dbName}`.".$ref.".id = ".$ref_id;
                $this->pdo->query($query);
                return true;
            } else {
                $query = "UPDATE `{$dbName}`.".$ref.
                         " SET `{$dbName}`.".$ref.".like_count = 
                                        CASE 
                                        WHEN `{$dbName}`.".$ref.".like_count > 0 
                                        THEN `{$dbName}`.".$ref.".like_count - 1 
                                        ELSE 0 
                                        END 
                                        WHERE `{$dbName}`.".$ref.".id = $ref_id";
                $this->pdo->query($query);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return false;
    }

    /**
     * Méthode pour gérer le vote "Je n'aime pas"
     * @param string $ref
     * @param int $ref_id
     * @param int $user_id
     * @return bool
     * @throws Exception
     */
    public function dislike($ref, $ref_id, $user_id): bool
    {
        global $dbName;
        if($this->vote($ref, $ref_id, $user_id,-1)){
            $sql_part = "";
            if(isset($this->former_vote)){
                if($this->former_vote){
                    $sql_part = ", `{$dbName}`.".$ref.".like_count = 
                    CASE 
                    WHEN `{$dbName}`.$ref.like_count > 0 
                    THEN `{$dbName}`.$ref.like_count - 1 
                    ELSE 0 
                    END";
                }
            }
            $query = "UPDATE `{$dbName}`.".$ref.
                     " SET `{$dbName}`.".$ref.".dislike_count = `{$dbName}`.".$ref.".dislike_count + 1".
                     $sql_part.
                     " WHERE `{$dbName}`.".$ref.".id = ".$ref_id;

            $this->pdo->query($query);
            return true;
        } else {
            $query = "UPDATE `{$dbName}`.".$ref.
                     " SET `{$dbName}`.".$ref.".dislike_count = 
                     CASE 
                     WHEN `{$dbName}`.".$ref.".dislike_count > 0 
                     THEN `{$dbName}`.".$ref.".dislike_count - 1 
                     ELSE 0 
                     END 
                     WHERE `{$dbName}`.".$ref.".id = $ref_id";
            $this->pdo->query($query);
        }
        return false;
    }


    /**
     * @throws Exception
     */
    private function vote($ref, $ref_id, $user_id, $vote): bool
        {
            global $dbName;
            $this->recordExists($ref, $ref_id);
            $req = $this->pdo->prepare("SELECT id, vote FROM `{$dbName}`.votes WHERE ref=? AND ref_id=? AND user_id=?");
            $req->execute([$ref, $ref_id, $user_id]);
            $vote_user = $req->fetch();
            if($vote_user){
                if($vote_user->vote == $vote){
                    $this->pdo->prepare("DELETE FROM `{$dbName}`.votes WHERE id=? AND ref=? AND ref_id =? AND user_id =?")->execute([$vote_user->id, $ref, $ref_id, $user_id]);
                    return false;
                }
                $this->former_vote = $vote_user;
                $this->pdo->prepare("UPDATE `{$dbName}`.votes SET vote = ?, created_at = ? WHERE id= $vote_user->id")->execute([$vote, date('Y-m-d H:i:s')]);
                return true;
            }
            $req = $this->pdo->prepare("INSERT into `{$dbName}`.votes SET ref_id = ?, ref = ?, user_id = ?, vote = ?, created_at = ?");
            $req->execute([$ref_id, $ref, $user_id, $vote, date('Y-m-d H:i:s')]);
            return true;
        }

    /**
     * Met à jour les compteurs de "likes" et "dislikes" pour un élément
     * @param string $ref
     * @param int $ref_id
     * @return true
     */
        public function updateCount($ref, $ref_id): bool
        {
            global $dbName;
            $req = $this->pdo->prepare("SELECT COUNT(id) as count, vote FROM `{$dbName}`.votes WHERE ref = ? AND ref_id=? GROUP BY vote  ");
            $req->execute([$ref, $ref_id]);
            $votes = $req->fetchAll();
            $counts = [
                '-1' => 0,
                '1' => 0
            ];

            foreach ($votes as $vote){
               $counts[$vote->vote] = $vote->count;
            }

            $req = $this->pdo->prepare("UPDATE `{$dbName}`.{$ref} SET like_count = ?, dislike_count = ? WHERE id = ?");
            $req->execute([$counts[1], $counts[-1], $ref_id]);
            return true;
        }




        /**
         * Permet d'obtenir la classe CSS en fonction du vote de l'utilisateur
         * @param mixed $vote false/PDORow
         * @return string|null
         */
        public static function getClass(mixed $vote): ?string
        {
            if($vote){
                return $vote->vote == 1 ? 'is-liked' : 'is-disliked';
            }
            return null;
        }

}