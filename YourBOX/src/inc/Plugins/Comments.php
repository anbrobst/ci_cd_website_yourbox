<?php

namespace Plugins;


use PDO;

require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
/**
 * Gestionnaire de commentaires.
 *
 * Cette classe gère la création, la suppression et la récupération des commentaires.
 *
 * @package Plugins
 */
class Comments {

    private PDO $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    /**
     * Supprime un commentaire et ses réponses associées.
     *
     * @param int    $id     L'ID du commentaire à supprimer.
     * @param string $ref    La référence du commentaire (ex: "articles").
     * @param int    $ref_id L'ID de la référence associée au commentaire.
     *
     * @return false|int Retourne le nombre de lignes affectées ou false en cas d'erreur.
     */
    public function delete($id, $ref, $ref_id): false|int
    {
        global $dbName;
        $comments= $this->findAll($ref, $ref_id);
        foreach ($comments as $comment){
            if ($comment->id == $id){
                $full_comment = $comment;
            }
        }
        $ids = [];
        if(isset($full_comment)) {
            foreach ($full_comment->replies as $reply) {
                $ids[] = $reply->id;
            }
        }

        $ids[] = $id;
        $_SESSION['flash']['success'] = 'Your comment was removed.';
        return $this->pdo->exec("DELETE FROM `{$dbName}`.comments WHERE id IN (" . implode(',', $ids) . ')');

    }
    /**
     * Récupère tous les commentaires associés à une référence et un ID donnés.
     *
     * @param string $ref    La référence du commentaire (ex: "articles").
     * @param int    $ref_id L'ID de la référence associée au commentaire.
     *
     * @return array Retourne un tableau de commentaires.
     */
    public function findAll($ref, $ref_id): array
    {
        global $dbName;
        $req=$this->pdo ->prepare("SELECT * FROM `{$dbName}`.comments WHERE ref_id = :ref_id AND ref= :ref ORDER BY created DESC" );
        $req ->execute(['ref_id' => $ref_id,'ref' => $ref]);
        $comments = $req->fetchAll(PDO::FETCH_OBJ);
        $replies = [];
        foreach ($comments as $k => $comment){
            if ($comment->parent_id){
                $replies[$comment->parent_id][] = $comment ;
                unset($comments[$k]);
            }
        }
        foreach ($comments as $comment){
            if (isset($replies[$comment->id])) {
                $r= $replies[$comment->id];
                usort($r, [$this, 'sortReplies']);
                $comment->replies = $r;
            }
        }
        return $comments;
    }

    /**
     * Fonction de comparaison pour trier les réponses par date de création décroissante.
     *
     * @param object $a Le premier objet de commentaire.
     * @param object $b Le deuxième objet de commentaire.
     *
     * @return int Retourne un entier négatif, zéro ou un entier positif selon que $a est considéré comme inférieur, égal ou supérieur à $b.
     */
    public function sortReplies($a, $b): int
    {
        $atime = strtotime($a->created);
        $btime = strtotime($b->created);

        return $btime > $atime ? -1 : +1;
    }

    /**
     * Enregistre un nouveau commentaire ou une réponse.
     *
     * @param string $ref    La référence du commentaire (ex: "articles").
     * @param int    $ref_id L'ID de la référence associée au commentaire.
     *
     * @return bool|array Retourne true si le commentaire est ajouté avec succès, sinon un tableau d'erreurs.
     */
    public function save($ref, $ref_id): bool|array
    {
        global $dbName;
        $errors = array();
        if(empty($_POST['content'])){
            $errors['content'] = "No content in your comment";
            $_SESSION['flash']['danger'] = "No content in your comment, please write something";
            header('Location: '.getenv('PROJECT_ROOT'). 'index.php');
            return $errors;
        }

        if (!empty($_POST['parent_id'])){
            $req = $this->pdo->prepare("SELECT id FROM `{$dbName}`.comments WHERE ref = ? AND ref_id = ?AND id= ? AND parent_id = 0 ");
            $req -> execute([$ref, $ref_id, $_POST['parent_id']]);

            if($req->rowCount()<= 0){
                $_SESSION['flash']['danger'] = "You reply to a comment that doesn't exist";
                header('Location: '.getenv('PROJECT_ROOT'). 'index.php');
                $errors['parent_id'] = "id of parent comment doesn't exist";
                return  $errors;
            }
        }
        if (isset($_SESSION['auth'])) {
            if (empty($errors)) {
                $req = $this->pdo->prepare("INSERT INTO `{$dbName}`.comments SET username= ?, email = ?,  content = ?, ref_id = ?, ref = ?, created = ?, parent_id = ? ");
                $_SESSION['flash']['success'] = 'Your comment was added.';
                return $req->execute([$_SESSION['auth']->username, $_SESSION['auth']->email, $_POST['content'], $ref_id, $ref, date('Y-m-d H:i:s'), $_POST['parent_id']]);
            }
        }
        return $errors;
    }
}