<?php


namespace Plugins;

use Exception;
use PDO;
use PDOStatement;

require_once getenv('PROJECT_ROOT') . 'src/inc/Database/db.php';



/**
 * Classe pour la gestion des requêtes paginées.
 *
 * Cette classe permet de paginer les résultats d'une requête SQL.
 *
 * @package Plugins
 */
class PaginatedQuery{
    private string $query;
    private string $queryCount;
    private PDO $pdo;
    private int $ElementsPerPage;

    /**
     * Constructeur de la classe PaginatedQuery.
     *
     * @param string $query        La requête SQL pour récupérer les éléments.
     * @param string $queryCount   La requête SQL pour compter le nombre total d'éléments.
     * @param PDO    $pdo          L'objet PDO pour la connexion à la base de données.
     * @param int    $elementsPerPage Le nombre d'éléments par page.
     */
    public function __construct( string $query, string $queryCount,$pdo, int $ElementsPerPage = 1){
        $this->query = $query;
        $this->queryCount = $queryCount;
        $this->pdo = $pdo ;
        $this->ElementsPerPage = $ElementsPerPage;

    }

    /**
     * Récupère les éléments de la page actuelle.
     *
     * @return false|PDOStatement Retourne l'objet PDOStatement ou false en cas d'erreur.
     *
     * @throws Exception En cas d'erreur lors de la récupération de la page.
     */
    public function getItems(): false|PDOStatement
    {

        $current_page = $this->getCurrentPage();
        $pages = $this->getPages();
        if($current_page > $pages){
            throw new Exception("This page doesn't exist");
        }
        $offset = $this->ElementsPerPage * ($current_page-1);
        return $this->pdo->query($this->query . " LIMIT $this->ElementsPerPage OFFSET $offset");
    }

    /**
     * Génère le lien vers la page précédente.
     *
     * @param string $link Le lien de base.
     *
     * @return string|null Retourne le code HTML du lien ou null s'il n'y a pas de page précédente.
     *
     * @throws Exception En cas d'erreur lors de la récupération de la page actuelle.
     */
    public function previousLink (string $link): ?string
    {
        $currentPage = $this->getCurrentPage();
        if($currentPage <= 1){
            return null;
        }
        if($currentPage >= 2) {$link .= "?page=" . ($currentPage-1);}
        return <<<HTML
                <a class="btn-page btn-primary" href="$link">&laquo; Previous page</a> 
        HTML;
    }

    /**
     * Génère le lien vers la page suivante.
     *
     * @param string $link Le lien de base.
     *
     * @return string|null Retourne le code HTML du lien ou null s'il n'y a pas de page suivante.
     *
     * @throws Exception En cas d'erreur lors de la récupération de la page actuelle.
     */
    public function nextLink (string $link): ?string
    {
        $currentPage = $this->getCurrentPage();
        $pages = $this->getPages();
        if($currentPage >= $pages){
            return null;
        }
        $link .= "?page=" . ($currentPage+1);
        return <<<HTML
                <a class="btn-page btn-primary" href="$link"> Next page &raquo;</a> 
        HTML;
    }

    /**
     * Récupère le numéro de la page actuelle.
     *
     * @return int Retourne le numéro de la page actuelle.
     *
     * @throws Exception En cas d'erreur si le numéro de page n'est pas valide.
     */
    public function getCurrentPage(): int {
        if(isset($_GET['page'])) {
            $current_page = (int)$_GET['page'] ?? 1;
        }
        else {
            $current_page = 1;
        }
        if(!filter_var($current_page, FILTER_VALIDATE_INT)){
            throw new Exception('The page number is not valid');
        }

        if($current_page <= 0){
            throw new Exception('The page number is not valid');
        }
        return $current_page;
    }

    /**
     * Récupère le nombre total de pages.
     *
     * @return int Retourne le nombre total de pages.
     *
     * @throws Exception En cas d'erreur lors de l'exécution de la requête de comptage.
     */
    public function getPages(): int {
        $result = $this->pdo->query($this->queryCount)->fetch(PDO::FETCH_NUM);
        if ($result === false) {
            throw new Exception("Failed to execute count query");
        }
        $count = (int)$result[0];
        return ceil($count / $this->ElementsPerPage);
    }

}

