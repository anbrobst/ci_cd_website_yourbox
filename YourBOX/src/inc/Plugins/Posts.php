<?php


namespace Plugins;


use Exception;
use PDO;
/**
 * Classe pour la gestion des publications (posts).
 *
 * Cette classe fournit des méthodes pour effectuer des opérations sur les publications.
 *
 * @package Plugins
 */
class Posts{
    private PDO $pdo;


    public function __construct($pdo){
        $this->pdo = $pdo;
    }


    /**
     * Met à jour une publication
     * @throws Exception
     */
    public function update($post): void
    {
        global $dbName;
        $query = $this->pdo->prepare("UPDATE `{$dbName}`.files SET name = ? WHERE id = ?");
        $ok = $query->execute([$post->name, $post->id]);
        if($ok === false){
            throw new Exception(" You can't remove this video $post->id");
        }
    }
}