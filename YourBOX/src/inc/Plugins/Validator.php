<?php

namespace Plugins;

require_once getenv('PROJECT_ROOT') . 'src/inc/Plugins/vendor/vlucas/valitron/src/Valitron/Validator.php';

use Valitron\Validator as ValitronValidator;

class Validator extends ValitronValidator {
    protected function checkAndSetLabel($field, $message, $params): array|string
    {

           return str_replace('{field}', '', $message);

    }

}