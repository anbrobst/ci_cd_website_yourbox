<?php
    if(session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    date_default_timezone_set('Europe/Paris');
    require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
    require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/styleHeader.css'?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <script src="<?= getenv('URL_root').'src/inc/JS/prototype.js'?>" type="text/javascript"></script>
    <script src="<?= getenv('URL_root').'src/inc/JS/jquery-3.5.1.min.js' ?>"></script>
    <link href="https://cdn.discordapp.com/attachments/1075707754272018480/1165643292197798029/Logo.png?ex=654798e9&is=653523e9&hm=f927685ec96720760a359d6956a535958b1faf2415a6c0a8143f82d5ece17399&" rel="icon"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div class="header">
    <div class="logo">
        <a href="<?= getenv('URL_root') . 'index.php' ?>">
                <strong>
                    <span class="Your">Your</span>
                    <span class="Box">BOX</span>
                </strong>
        </a>
    </div>
    <form autocomplete="on" id="search_form" class="search_form" action="<?= getenv('URL_root').'src/searching/answer_request.php'?>"  method="post">
        <div class="search-box">
            <input type="text" name="mc" class="search-txt" placeholder="Search..."/>
            <input type="text" name="fake_button" style="display: none">
            <button type="submit" style="border: none" class="search-btn"  >
                <i class="fas fa-search"></i>
            </button>
        </div>
    </form>
    <div class="UpperRightBoxes">
        <div class="UpperRightBox">
            <p>VIP</p>
        </div>
        <div class="UpperRightBox">
            <p>Box</p>
        </div>
        <div class="UpperRightBox">
            <p>Challenge</p>
        </div>
    </div>
    <div class="UpperRightBoxes">
        <?php if (isset($_SESSION['auth'])):?>
            <div class="UpperRightBox">
                <a href="<?= getenv('URL_root').'src/accounts/modification/modify_profile.php'?>"><p>My Account</p></a>
            </div>
            <div class="UpperRightBox">
                <a href="<?= getenv('URL_root').'src/accounts/login/logout.php'?>"><p>Log out</p></a>
            </div>
        <?php else: ?>
            <div class="UpperRightBox">
                <a href="<?= getenv('URL_root').'src/accounts/login_registration.php'?>"><p>Member space</p></a>
            </div>
        <?php endif; ?>
    </div>
</div>
</body>
</html>

