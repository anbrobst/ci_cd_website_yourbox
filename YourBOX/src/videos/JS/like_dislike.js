(function ($) {

    let $vote_like = $('#button_like');
    let $vote_dislike = $('#button_dislike')
    let $URL_root = "https://" + window.location.hostname + "/";
    $vote_like.click(function (e){
        e.preventDefault();
        vote_video(1, $URL_root);

    });
    $vote_dislike.click(function (e){
        e.preventDefault();
        vote_video(-1, $URL_root);

    });


    function vote_video(value, URL_root){
        $.post(URL_root+'src/videos/PHP/liking/like.php' , {
            ref: $vote_like.data('ref'),
            ref_id: $vote_like.data('ref_id'),
            user_id: $vote_like.data('user_id'),
            vote: value
        }).done(function(data){
            $('#dislike_count').text(data.dislike_count);
            $('#like_count').text(data.like_count);
            $vote_like.removeClass('is-liked');
            $vote_dislike.removeClass('is-disliked');
            if(data.success) {
                if (value === 1) {
                    $vote_like.addClass('is-liked');
                    alert("Your video like is saved");

                } else {
                    $vote_dislike.addClass('is-disliked');
                    alert("Your video dislike is saved");
                }
            } else if(!data.success){
                if (value=== 1){
                    alert("Your like has been deleted");
                } else {
                    alert("Your dislike has been deleted");
                }
            }

            let percentage_like = Math.round(100*((data.like_count) / (parseInt(data.dislike_count) + parseInt(data.like_count))));
            let percentage_dislike = Math.round(100*((data.dislike_count) / (parseInt(data.dislike_count) + parseInt(data.like_count))));
            $('.rating_progress_bar_up').css('width', percentage_like + '%');
            $('.rating_progress_bar_down').css('width', percentage_dislike + '%');
        }).fail(function (jqXHR){
            alert(jqXHR.responseText);

        });
    }


}(jQuery));

