<?php

global $pdo, $dbName;

use Plugins\Vote;



require_once getenv('PROJECT_ROOT') . 'src/inc/Database/db.php';

$accepted_refs = ['files', 'comments'];

if($_SERVER['REQUEST_METHOD'] != 'POST'){
    http_response_code(403);
    die();
}

if(!in_array($_POST['ref'], $accepted_refs)){
    http_response_code(403);
    die();
}


if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if(!isset($_SESSION['auth'])){
    http_response_code(403);
    die("You must be logged in to vote.");
}

require_once getenv('PROJECT_ROOT') . 'src/inc/Plugins/Vote.php';

$vote = new Vote($pdo);

if ($_POST['vote'] == 1){
    $success= $vote->like($_POST['ref'], $_POST['ref_id'], $_SESSION['auth']->id);
}
else  {
    try {
        $success = $vote->dislike($_POST['ref'], $_POST['ref_id'], $_SESSION['auth']->id);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

$query = "SELECT `{$dbName}`.{$_POST['ref']}.like_count, `{$dbName}`.{$_POST['ref']}.dislike_count 
          FROM `{$dbName}`.{$_POST['ref']} WHERE `{$dbName}`.{$_POST['ref']}.id = ? ";

$req = $pdo->prepare($query);
$req->execute([$_POST['ref_id']]);
header('Content-Type: application/json');
$record = $req->fetch(PDO::FETCH_ASSOC);
if (isset($success)):
    $record['success'] = $success;
endif;
die(json_encode($record));