<?php

require_once getenv('PROJECT_ROOT') . 'src/inc/header.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/menu_nav.php';
require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
global $pdo;
?>
<link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/upload_video.css'?>">
<div class="content_section_content content_upload main">
    <div id="introduction">
        <p>The YourBox allow you to share your videos !</p>
    </div>
    <div class="pool_uploading_explanation">
        <p>
            <strong>
                Before uploading a video to YourBOX, please ensure that you are not infringing on <br>the copyrights or privacy of others.<br/> By making an upload to YourBox, you guarantee that all parties involved in the content consent to it.
                By making an upload to YourBox, you warrant that all parties in the content consent to it, that you do so with full knowledge of the facts, and that you are prepared to provide any information about it
                upon request. You also agree to indemnify and hold harmless YourBox from any claims resulting from your uploads.
            </strong>
        </p>
        <p><strong>Once registered, you can send your videos using our upload form.</strong></p>
    </div>
    <form
            id="upload_form"
            action="upload_video.php"
            enctype="multipart/form-data"
            method="post"
    >
        <input type="hidden" id="progress_id" name="PHP_SESSION_UPLOAD_PROGRESS" value="5f27f8288eb8c" />
        <div style="display: block;max-width: 750px;">
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item">
                            <label>
                                Main Category
                            </label>
                        </div>
                        <div class="form_item">
                            <label for="main_category">
                                <select id="main_category" name="main_category" class="form-control">
                                    <option value="0">Select</option>
                                    <option value="Games">Games</option>
                                    <option value="Vlog">Vlog</option>
                                    <option value="DIY">DIY</option>
                                    <option value="Tutorial">Tutorial</option>
                                    <option value="Music">Music</option>
                                    <option value="Sport">Sport</option>
                                    <option value="Humor">Humor</option>
                                    <option value="Animals">Animals</option>
                                    <option value="Movies">Movies</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item">
                            <label for="title" style="background-position-y:255px ">Title
                                <span class="title_counter char_counter form-control-feedback warning">
                                  70 characters max
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="form_item">
                            <div class="emojiPickerIconWrap">
                                <input type="text" autocomplete="off" id="title" name="title" class="form-control"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item">
                            <label for="description">
                                Description
                                <span class="description_counter char_counter form-control-feedback warning">
                                  155 characters max
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="row">
                        <div class="form_item">
                            <div class="emojiPickerIconWrap">
                                <label for="description">
                                    <textarea id="description" name="description" class="form-control" style="width: 80%"></textarea>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item">
                            Creators
                        </div>
                        <div class="form_item">
                            <div class="no_chosen_items" style="color: #919191; display: none">
                                No creators chosen
                            </div>
                            <label for="creator">
                                <select id="creator" name="creator" class="js_creator_select form-control" multiple="" size="13">
                                    <option value="0">--New--</option>
                                    <?php
                                    $req = $pdo->query("SELECT DISTINCT creator FROM `{$dbName}`.files");
                                    for($i=0;$i<$req->rowCount();$i++)
                                    {
                                        $creator = $req->fetch()->creator;
                                        echo '<option value="'.$creator.'">';
                                        echo $creator;
                                        echo "</option>";
                                    }
                                    ?>
                                </select>
                            </label>
                            <div class="add_creators">
                                <label for="custom_creator">
                                    <input type="text" id="custom_creator" name="custom_creator" class="form-control" />
                                </label>
                                <small>
                                    Enter new creator.
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item form_toc">
                            <div class="form_toc_check"><input type="checkbox" class="form-control" id="toc" name="toc" value="Yes" /></div>
                            <div class="form_toc_label">
                                <label for="toc" class="toc_label">I accept the <a target="_blank" href="">terms and conditions</a>.</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item"></div>
                        <div class="form_item">
                            Choose your file : <input type="file" name='video_tmp'><br/>
                            Choose a thumbnail for your video : <input type="file" name='thumbnail'><br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div>
                    <div class="row form-group">
                        <div class="form_item form_submit">
                            <input type="submit" class="form-control btn btn-outline-dark" id="upload_video" value="Send"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function() {
        const uploadForm = document.getElementById("upload_form");

        uploadForm.addEventListener("submit", function(event) {
            let hasError = false;

            // Vérification du champ "creator"
            const creatorSelect = document.getElementById("creator");
            const customCreatorInput = document.getElementById("custom_creator");
            if ( (creatorSelect.value === "0") || (creatorSelect.selectedOptions.length === 0) ){
                if(customCreatorInput.value.trim() === "") {
                    alert("Please choose an creator.");
                    hasError = true;
                }
            }

            // Vérification du champ "main_category"
            const mainCategorySelect = document.getElementById("main_category");
            if (mainCategorySelect.value === "0") {
                alert("Please select a main category.");
                hasError = true;
            }

            // Vérification du champ "description"
            const descriptionTextarea = document.getElementById("description");
            if (descriptionTextarea.value.trim() === "") {
                alert("Please enter a description.");
                hasError = true;
            }

            // Vérification du champ "thumbnail"
            const thumbnailInput = uploadForm.querySelector("input[name='thumbnail']");
            if (!thumbnailInput.files.length) {
                alert("Please choose a thumbnail for your video.");
                hasError = true;
            }

            // Vérification du champ "video_tmp"
            const videoTmpInput = uploadForm.querySelector("input[name='video_tmp']");
            if (!videoTmpInput.files.length) {
                alert("Please choose a video file.");
                hasError = true;
            }

            // Vérification du champ "title"
            const titleInput = document.getElementById("title");
            if (titleInput.value.trim() === "") {
                alert("Please enter a title.");
                hasError = true;
            }

            // Vérification du champ "title"
            const ToS_checkbox = document.getElementById("toc");
            if (!ToS_checkbox.checked) {
                alert("Please accept terms and conditions.");
                hasError = true;
            }

            // If an error is found, prevent form submission
            if (hasError) {
                event.preventDefault();
            }
        });
    });
</script>

