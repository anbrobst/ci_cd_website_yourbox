<?php
global $pdo, $dbName;

require_once getenv('PROJECT_ROOT') . 'src/inc/Database/db.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/functions.php';


if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
//var_dump($_POST);
//var_dump($_FILES);
$user_id = $_SESSION['auth']->id;
$creator = "0";
if(isset($_POST['creator']))
{
    $creator = $_POST['creator'];
}
$custom_creator = "0";
if(isset($_POST['custom_creator']))
{
    $custom_creator = $_POST['custom_creator'];
}
$used_creator = "0";
$main_category = $_POST['main_category'];
$description = $_POST['description'];
$name_thumbnail = $_FILES['thumbnail']['name'];
$folder = getenv('PROJECT_ROOT') . 'cache/upload/';
$name_file = $_FILES['video_tmp']['name'];
$title_video = $_POST['title'];
$dest_file = $folder . $name_file;
$dest_thumbnail = $folder . $name_thumbnail;
$size_maxi = 500000000;
$size = filesize($_FILES['video_tmp']['tmp_name']);
$extensions = array('.mp4', '.webm', '.avi', ".mov");
$extension = strrchr($_FILES['video_tmp']['name'], '.');
//print_r($dest_file);
if (!empty($_POST)){
    $errors = array();
    $req = $pdo->prepare("SELECT id FROM `{$dbName}`.files WHERE `{$dbName}`.files.name = ?");
    $req -> execute([$name_file]);
    $file_taken = $req->fetch();
    if($file_taken){
        $errors['files_taken'] = 'This file is already uploaded';
    }
    if(empty($errors)){


        if(!empty($_FILES)) {
            if (!in_array($extension, $extensions)) {
                $_SESSION['flash']['danger'] = "Wrong format of file, those allowed are : avi, mp4, mov et webm";
                header('Location: '.getenv('URL_root').'index.php');
                exit();

            }
            if ($size > $size_maxi) {
                $error = "The file is too big, 2Gb max";
                exit();
            }
            if (!isset($error))
            {

                $name_file = strtr($name_file,
                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                $name_file = preg_replace('/([^.a-z0-9]+)/i', '-', $name_file);
                $title_video = strtr($title_video,
                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                $title_video = preg_replace('/([^.a-z0-9]+)/i', '-', $title_video);
                $name_thumbnail = strtr($name_thumbnail,
                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
                $name_thumbnail = preg_replace('/([^.a-z0-9]+)/i', '-', $name_thumbnail);

                if (move_uploaded_file($_FILES['video_tmp']['tmp_name'], $dest_file) && move_uploaded_file($_FILES['thumbnail']['tmp_name'], $dest_thumbnail))
                {
                    $vid = $dest_file;
                    if (file_exists($vid)) {
                        $finfo = finfo_open(FILEINFO_MIME_TYPE);
                        $mime_type = finfo_file($finfo, $vid); // check mime type
                        finfo_close($finfo);

                        if (preg_match('/video\/*/', $mime_type)) {
                            global $video_attributes;
                            $video_attributes = _get_video_attributes($vid);
                            global $duration, $hours, $mins, $secs;
                            $duration = explode(':', $video_attributes['duration']);
                            $hours = $duration[0];
                            $mins = $duration[1];
                            $secs = $duration[2];
                            if ($hours == 0){
                                $video_attributes['duration'] = $mins . ':' . $secs;
                            } else{
                                $video_attributes['duration'] = $hours . ':' . $mins . ':' . $secs;
                            }

                            //print_r('Resolution: ' . $video_attributes['resolution']  .'pixels'.'<br/>');

                            //print_r('Duration: ' . $video_attributes['duration'] . '<br/>');

                            //print_r('Size:  ' . _human_filesize(filesize($vid)));

                        } else {
                            $_SESSION['flash']['danger'] = 'File is not a video.';
                            exit();
                        }
                    } else {
                        $_SESSION['flash']['danger'] = 'File does not exist.';
                        exit();
                    }

                    if ($creator === "0")
                    {
                        $used_creator = $custom_creator;
                    }
                    else{
                        $used_creator = $creator;
                    }

                    $req = $pdo->prepare("INSERT INTO `{$dbName}`.files(name, complete_name, file_url, size, duration, date_upload, URL_preview, resolution, sender_id, description, creator, main_category) VALUES(?, ?,?,?,?,NOW(), ?, ?, ?, ?, ?, ?)");
                    $req ->execute([$title_video, $name_file, $dest_file, $size,$video_attributes['duration'], $dest_thumbnail, $video_attributes['resolution'], $user_id, $description, $used_creator, $main_category]);
                    $_SESSION['flash']['success'] = "Upload done !";
                }else
                {
                    $_SESSION['flash']['danger'] = "Failure of Upload";
                }
                header('Location: '.getenv('URL_root').'index.php');
                exit();
            } else {
                echo $error;
            }
            exit();
        }
    }

}




