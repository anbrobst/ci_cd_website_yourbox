<?php

global $dbName;
require_once getenv('PROJECT_ROOT') . 'src/inc/Database/db.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/functions.php';
global $pdo;
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}
$id = $_GET['id'];

$req = $pdo->query("SELECT * FROM `{$dbName}`.files WHERE id = $id ");
global $video_preview;
$video_preview = $req->fetch();
$path_thumbnail = $video_preview->URL_preview;
global $thumbnail, $title_video, $video;
$thumbnail = basename($path_thumbnail);
$path_video = $video_preview->file_url ;
$video = basename($path_video);
$title_video = pathinfo("$path_video");
$title_video = $title_video ['filename'];
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <title>video player</title>

</head>
<body>

    <link rel="stylesheet" href="<?= getenv('URL_root') . 'src/assets/stylesheets/player.css' ?>">


        <video  style="height: 98%;" controls class="vjs-tech" id="video1"
                src="<?= getenv('URL_root') . 'cache/upload/' .$GLOBALS['video'] ?>"
                poster="<?= getenv('URL_root') . 'cache/upload/' .$GLOBALS['thumbnail']?>" ></video>

</body>
</html>
