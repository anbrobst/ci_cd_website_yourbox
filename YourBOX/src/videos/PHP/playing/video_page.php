<?php

global $pdo, $dbName;

require_once getenv('PROJECT_ROOT') . 'src/inc/functions.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/Database/db.php';



$id = $_GET['id'];
$req = $pdo->query("SELECT * FROM `{$dbName}`.files WHERE id = $id ");
$video_info = $req->fetch();
$name_video = $video_info->name;
$title_video = pathinfo("$name_video");
$title_video = $title_video ['filename'];
$number_views = $video_info->number_of_views;
if ($number_views == NULL){
    $number_views = 1;
}
else {
    $number_views ++;
}

$request = $pdo->prepare("UPDATE `{$dbName}`.files SET `{$dbName}`.files.number_of_views = ? WHERE id= $id");
$request->execute(["$number_views"]);



if(!isset($_GET['id'])){
    $_SESSION['flash']['danger'] = "You didn't choose a video";
    header('Location: '.getenv('URL_root').'index.php');
    exit();
}


if(!preg_match('/^([a-z0-9A-Z]+\.?)+$/', $_GET['id'])){
    $page=  'src/comments/errors/404';
}else {
    $page = 'src/comments/views/posts/view';

}

//load comments in content if they exists

ob_start();
if (file_exists(getenv('PROJECT_ROOT') . $page . '.php' )) {
    try {
        require getenv('PROJECT_ROOT') . $page . '.php';
    } catch (Exception $e) {
        debug($e);
        exit('Error');
    }

}else {
    require getenv('PROJECT_ROOT') . 'src/comments/errors/404.php';
}
$content = ob_get_clean();

require_once getenv('PROJECT_ROOT') . 'src/comments/views/layout/default.php';



