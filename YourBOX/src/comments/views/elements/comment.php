<?php
/**
 * Affichage d'un commentaire.
 *
 * Cette partie du script affiche un commentaire, permettant à un utilisateur de voir les détails d'un commentaire spécifique.
 * Elle inclut les informations sur l'auteur du commentaire, la date de création, le contenu du commentaire et les actions disponibles (j'aime, je n'aime pas, répondre, supprimer).
 * La gestion des votes est également intégrée, permettant aux utilisateurs de voter pour ou contre un commentaire.
 *
 * @global PDO    $pdo          L'objet de connexion à la base de données global.
 * @global string $dbName       Le nom de la base de données global.
 * @global object $comment      L'objet du commentaire à afficher.
 */

global $pdo, $dbName;
global $comment;

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

use Plugins\Vote;

$vote_comments = false;


if(isset($_SESSION['auth'])){
    $id_user = $_SESSION['auth']->id;
    $req_comments = $pdo->prepare("SELECT * FROM `{$dbName}`.votes WHERE ref = ? AND ref_id = ? AND user_id = ?");
    $req_comments->execute(['comments',$comment->id, $id_user]);
    $vote_comments = $req_comments->fetch();
}

?>
<div id = "main_comment" class="comments_wrapper js_comments_wrapper">
    <div id="comment_<?= $comment-> id?>" class="clearfix comment comment_highlighted" data-comment_id="<?= $comment-> id?>">
        <div class="comment_avatar_wrapper">
            <img height="30px" width="30px" class="img-responsive" src="https://www.gravatar.com/avatar/<?= md5($comment->email);?>" alt="avatar" />
            <p class="comment_item_role">Guest</p>
        </div>
        <div class="comment_body_wrapper">
            <div class="comment_author_wrapper">
                <strong>
                    <?= $comment -> username ?>
                </strong>
                <span class="comment_item_date">
                    - <?= date('d/m/Y', strtotime($comment->created))?> - <?= date('H:i:s', strtotime($comment->created))?>
                </span>
            </div>
            <div class="comment_wrapper">
                <?= $comment->content?>
            </div>
            <div id="comment_actions_wrapper_<?= $comment->id?>" class="comment_actions_wrapper">
                <div data-ref="comments" data-user_id="<?= $_SESSION['auth']->id ?? '' ?>" data-ref_id="<?= $comment->id ?>" id="button_like_comment_<?= $comment->id ?>" class="comment_action_item js_comment_vote_up custom-icon-parent <?= Vote::getClass($vote_comments)?> " >
                    <i class="fas fa-thumbs-up"></i>
                    <span  id="like_count_comment_<?= $comment->id ?>" class="js_comment_vote_up_txt "><?= $comment->like_count?></span>
                </div>
                <div data-ref="comments" data-user_id="<?= $_SESSION['auth']->id ?? '' ?>" data-ref_id="<?= $comment->id ?>" id="button_dislike_comment_<?= $comment->id ?>" class="comment_action_item js_comment_vote_down custom-icon-parent <?= Vote::getClass($vote_comments)?> ">
                    <i class="fas fa-thumbs-down"></i>
                    <span id="dislike_count_comment_<?= $comment->id ?>" class="js_comment_vote_down_txt"><?= $comment->dislike_count?></span>
                </div>
                <div class="comment_action_item custom-icon-parent">
                    <span id="reply_<?= $comment->id?>" data-id="<?= $comment->parent_id ?: $comment->id  ?>" class="answer" onclick="">Answer</span>
                </div>
                <?php if($_SESSION['auth']->username == $comment->username): ?>
                    <div class="comment_action_item custom-icon-parent">
                        <form action='' id='comment' method='post'>
                            <input type="hidden" name="id_comment" value="<?= $comment->id ?>">
                            <input type="hidden" name="action" value="remove" >
                            <input  type="submit" style="padding: 0; font-size: 12px" id="remove_<?= $comment->id?>" value="Remove"  class="remove answer">
                        </form>
                    </div>
                <?php endif; ?>
                <?php if(isset($comment->replies)): ?>
                    <div  id="show_reply_<?= $comment->id?>" class=" show_reply comment_action_item comment_show_replies js_comment_show_replies"><span>Show Answer(s)</span><i class="fas fa-sort-down"></i></div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>