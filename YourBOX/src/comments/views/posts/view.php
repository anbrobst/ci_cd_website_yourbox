<?php
global $pdo;

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

$dbName = getenv('DB_name');

$req = $pdo -> prepare("SELECT * FROM `{$dbName}`.files WHERE id = ?");

$req ->execute([$_GET['id']]);

try {
    $post = $req-> fetch();
} catch (Exception $e) {
    echo $e->getMessage();
}

require_once getenv('PROJECT_ROOT').'src/inc/functions.php';
require_once getenv('PROJECT_ROOT').'src/inc/Plugins/Comments.php';
require_once getenv('PROJECT_ROOT').'src/inc/Plugins/Vote.php';

use Plugins\Comments;
use Plugins\Vote;

$comments_cls = new Comments($pdo);

if(isset($_POST['action']) && ($_POST['action'] == 'reply' || $_POST['action'] == 'comment') ){
    $comments_cls->save('files', $post->id);//'posts' est le référent
    header('Location: '.getenv('URL_root').'src/videos/PHP/playing/video_page.php?id='.$post->id);
    exit();

}


if(isset($_POST['action']) && $_POST['action'] == 'remove'){
    $comments_cls->delete($_POST['id_comment'], 'files', $post->id);
    header('Location: '.getenv('URL_root').'src/videos/PHP/playing/video_page.php?id='.$post->id);
    exit();
}

$comments = $comments_cls->findAll('files', $post->id);

?>

<div class="video_comments_wrapper">
    <header>Post your comment</header>
    <div class="comments_display_wrapper">
        <form action="" id="comment" method="post">
            <div class="comments_new_comment_wrapper comments_recaptcha_enabled">
                <div id='emojiPickerIconWrap' class="emojiPickerIconWrap">
                    <label>
                        <textarea placeholder="Your comment" required class="comments_new_comment_txt js_new_comment_input emojiPickerInput" name="content" style="padding-right:40px;"></textarea>
                    </label>
                    <i class="far fa-smile-beam"></i>
                </div>
            </div>
            <?php if(isset($_SESSION['auth'])): ?>
            <div class="comments_new_comment_btn">
                <input type="hidden" name="parent_id" value="0" id="parent_id_0">
                <input type="hidden" name="action" value="comment" >
                <button type="submit" class="js_new_comment  btn_default btn_comments_new" >Post</button>
            </div>
            <?php else: ?>
            <div class="comments_new_comment_btn">
                <button class="js_new_comment  btn_default btn_comments_new" type="submit">Please sign in to post a comment.</button>
            </div>
            <?php endif; ?>
        </form>
        <div class="comments_section_wrapper js_comments_section_wrapper">
            <div class="comments_top_title">
                <i class="fa fa-comments" aria-hidden="true"></i>
                <span><?= count($comments)?> Comments</span>
            </div>
            <?php foreach ($comments as $comment):?>
                <?php require  getenv('PROJECT_ROOT') .'src/comments/views/elements/comment.php'; ?>
                <?php if (isset($comment->replies)): ?>
                    <?php $array = array_keys($comment->replies);
                    $lastKey = array_pop($array); ?>
                    <?php foreach ($comment->replies as $key => $reply): ?>
                       <?php

                        $vote_comments = false;


                        if(isset($_SESSION['auth'])){
                        $id_user = $_SESSION['auth']->id;
                        $req_comments = $pdo->prepare("SELECT * FROM `{$dbName}`.votes WHERE ref = ? AND ref_id = ? AND user_id = ?");
                        $req_comments->execute(['comments',$reply->id, $id_user]);
                        $vote_reply = $req_comments->fetch();
                        }?>

                        <script>

                            document.getElementById('comment_actions_wrapper_<?= $comment->id?>').innerHTML += "<div id =\"comment_reply_wrapper_<?= $comment->id?>\" class=\"comment_reply_wrapper comment_reply_wrapper_<?= $comment->id?>\" >\n" +
                                "    <div class=\"comment_replies\">\n" +
                                "        <div id=\"comment_723536\" class=\"clearfix reply comment_reply\" data-comment_id=\"723536\" data-parent_entity_id=\"690498\" data-parent_entity_type_id=\"5\" data-is_child=\"true\">\n" +
                                "            <div class=\"comment_avatar_wrapper\">\n" +
                                "                <img  height=\"30px\" width=\"30px\" class=\"img-responsive\" src=\"https://www.gravatar.com/avatar/<?= md5($reply->email);?>\" alt=\"avatar\">                <p class=\"comment_item_role\">Guest</p>\n" +
                                "            </div>\n" +
                                "            <div class=\"comment_body_wrapper\">\n" +
                                "                <div class=\"comment_author_wrapper\">\n" +
                                "                    <strong><?= $reply->username ?></strong>\n" +
                                "                    <span class=\"comment_item_date\">- <?= date('d/m/Y', strtotime($reply->created))?> - <?= date('H:i:s', strtotime($reply->created))?></span>\n" +
                                "                </div>\n" +
                                "                <div class=\"comment_wrapper\"><?= $reply->content?></div>\n" +
                                "                <div class=\"comment_actions_wrapper\">\n" +
                                "                   <div data-ref=\"comments\" data-user_id=\"<?= $_SESSION['auth']->id ?? '' ?>\"  data-ref_id=\"<?= $reply->id ?>\" id=\"button_like_reply_<?= $reply->id ?>\" class=\"comment_action_item js_comment_vote_up custom-icon-parent <?php global $vote_reply; echo Vote::getClass($vote_reply)?> \" >\n" +
                                "                       <i class=\"fas fa-thumbs-up\"></i>\n" +
                                "                       <span  id=\"like_count_reply_<?= $reply->id ?>\" class=\"js_comment_vote_up_txt \"><?= $reply->like_count?></span>\n" +
                                "                   </div>\n" +
                                "                <div data-ref=\"comments\" data-user_id=\"<?= $_SESSION['auth']->id ?? '' ?>\"  data-ref_id=\"<?= $reply->id ?>\" id=\"button_dislike_reply_<?= $reply->id ?>\" class=\"comment_action_item js_comment_vote_down custom-icon-parent <?php global $vote_reply; echo Vote::getClass($vote_reply)?> \">\n" +
                                "                    <i class=\"fas fa-thumbs-down\"></i>\n" +
                                "                    <span id=\"dislike_count_reply_<?= $reply->id ?>\" class=\"js_comment_vote_down_txt\"><?= $reply->dislike_count?></span>\n" +
                                "                </div>" +
                                "                    <div class=\"comment_action_item custom-icon-parent js_comment_report\">\n" +
                                "                        <span id=\"reply_<?= $reply->id?>\" data-id=\"<?= $comment->parent_id ?: $comment->id  ?>\" class=\"answer\" onclick=\"\">Answer</span>\n" +
                                "                    </div>\n" +
                                    <?php if($_SESSION['auth']->username == $reply->username): ?>
                                                     "<div style='height: 17px;' class='comment_action_item custom-icon-parent'>\n"+
                                "                   <form action='' id='comment' method='post'>            " +
                                "                        <input type='hidden' name='id_comment' value='<?= $reply->id ?>' >" +
                                "                        <input type=\"hidden\" name=\"action\" value=\"remove\" >"+
                                                        "<input  type=\"submit\" id=\"remove_<?= $comment->id?>\" value=\"Remove\"  class=\"remove answer\">"+
                                "                     </form>            "  +
                                    <?php endif; ?>

                            "                </div>\n" +
                                "            </div>\n" +
                                "        </div>\n" +
                                "    </div>\n" +
                                <?php  if ($key === $lastKey) : ?>
                                "    <form method='post' action='' id='comment'>"+
                                "     <div id='comment_reply_input_<?= $reply->id?>' class=\"comment_reply_input comment_reply_input_<?= $reply->id?>\">\n" +
                                "        <div class=\"comments_reply_comment_wrapper comments_recaptcha_enabled\">\n" +
                                "            <div  class=\"emojiPickerIconWrap \">\n" +
                                "                <textarea placeholder='Your answer' name ='content' class=\"comments_new_comment_txt js_new_reply_input emojiPickerInput\" style=\"padding-right: 40px;\"></textarea>\n" +
                                "                <i class=\"far fa-smile-beam\"></i>\n" +
                                "            </div>\n" +
                                "        </div>\n" +

                                <?php if(isset($_SESSION['auth'])): ?>
                                "      <div class='comments_new_comment_btn'>"+
                                "           <button type='submit' class='js_new_comment  btn_default btn_comments_new' type='submit'>Post</button>"+
                                "       </div>"+

                                <?php else: ?>
                                "       <div class='comments_new_comment_btn'>"+
                                            "<button type='button' class='js_new_comment  btn_default btn_comments_new' type='submit'>Please login to post a comment</button>"+
                                "        </div>"+
                                <?php endif; ?>
                                "       <input type='hidden' name='parent_id' value='<?= $comment->id?>' id='parent_id_<?= $comment->id?>'>"+
                                "        <input type='hidden' name='action' value='comment' >" +
                                "   </form>"+
                                <?php endif; ?>
                                "    </div>\n" +
                                "</div>\n" +
                                "\n"

                        </script>
                    <?php endforeach; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>

</div>

