<?php
global $pdo, $title_video, $video_info, $comments, $id, $content, $dbName;

use Plugins\Vote;


require_once getenv('PROJECT_ROOT').'src/inc/Plugins/Vote.php';

$vote_comments = false;
$vote_video = false;
if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if(isset($_SESSION['auth'])){
    $id= $_GET['id'];
    $id_user = $_SESSION['auth']->id;
    $req_video = $pdo->prepare("SELECT * FROM `{$dbName}`.votes WHERE ref = ? AND ref_id = ? AND user_id = ?");
    $req_video->execute(['files',$_GET['id'], $id_user]);
    $vote_video = $req_video->fetch();
    $req_comments = $pdo->prepare("SELECT * FROM `{$dbName}`.votes WHERE ref = ? AND ref_id = ? AND user_id = ?");
    $req_comments->execute(['comments',$_GET['id'], $id_user]);
    $vote_comments = $req_comments->fetch();
    $video_info = $pdo->query("SELECT * FROM `{$dbName}`.files WHERE id = $id")->fetch();
    $title_video = $video_info->name;




} else{
    $_SESSION['flash']['danger'] = "you need to be logged in to access this page";
    header('Location: '.getenv('URL_root').'src/accounts/login_registration.php');
    exit();

}
require_once getenv('PROJECT_ROOT') . 'src/inc/header.php';
require_once getenv('PROJECT_ROOT') . 'src/inc/menu_nav.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Comments</title>
    <link rel="stylesheet" href=" <?= getenv('URL_root').'src/assets/stylesheets/video_page.css' ?>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

</head>
<body >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>jQuery.noConflict();</script>
<?php require_once getenv('PROJECT_ROOT').'src/inc/menu_nav.php'; ?>
<div class = "main">
    <header><?=$title_video?></header>
    <iframe src="<?= getenv('URL_root') . 'src/videos/PHP/playing/player.php?id=' .$id?>" height="468px" width="833px" ></iframe>


    <div id ="menu" style="padding :10px 16px" class=" -menu-renderer style-scope video-primary-info-renderer">
        <div class="menu-renderer style-scope ytd-menu-renderer force-icon-button style-text">
            <div id="top-level-buttons" class="style-scope menu-renderer">
                <div class="toggle-button-renderer style-scope menu-renderer force-icon-button style-text " use-keyboard-focused="" button-renderer="true" is-icon-button="">
                    <a class="simple-endpoint style-scope toggle-button-renderer" tabindex="-1">
                        <div id="button"  class="icon-button style-scope toggle-button-renderer style-text">
                            <button id="button_like" data-ref="files" data-user_id="<?= $_SESSION['auth']->id ?? '' ?>" data-ref_id="<?= $id ?>" class="style-scope -icon-button <?= Vote::getClass($vote_video)?>" aria-label="Click to like this video" aria-pressed="false">
                                <div class="icon style-scope toggle-button-renderer  ">
                                    <i style="font-size: 22px" class=" sx-icon far fa-thumbs-up"></i>
                                </div>
                            </button>
                        </div>
                        <div  id="like_count" style="text-align: center" class="formatted-string style-scope -toggle-button-renderer style-text" aria-label="So much like"><?= $video_info->like_count?></div>
                    </a>
                </div>
                <div class="rating js_rating_wrapper">
                    <div class="rating_progress">
                        <div class="rating_progress_bar rating_progress_bar_up js_number_up none" style="width: <?= ($video_info->like_count + $video_info->dislike_count) == 0 ? 100 : round(100 * (($video_info->like_count)/($video_info->like_count + $video_info->dislike_count))) ?>%"></div>
                        <div class="rating_progress_bar rating_progress_bar_down js_number_down none" style="width: <?= ($video_info->like_count + $video_info->dislike_count) == 0 ? 0 : round(100 * (($video_info->dislike_count)/($video_info->like_count + $video_info->dislike_count))) ?>%"></div>
                    </div>
                </div>
                <div class="toggle-button-renderer style-scope menu-renderer force-icon-button style-text " use-keyboard-focused="" button-renderer="true" is-icon-button="">
                    <a class="simple-endpoint style-scope toggle-button-renderer" tabindex="-1">
                        <div id="button" data-ref="files" data-id="<?= $id ?>" class="icon-button style-scope toggle-button-renderer style-text">
                            <button data-ref="files" data-user_id="<?= $_SESSION['auth']->id ?? '' ?>" data-ref_id="<?= $id ?>" id="button_dislike" class="style-scope -icon-button <?= Vote::getClass($vote_video)?>" aria-label="Click on dislike but it's mean for the author :(" aria-pressed="false">
                                <div  class="icon style-scope toggle-button-renderer ">
                                    <i style="font-size: 22px" class=" sx-icon far fa-thumbs-down"></i>
                                </div>
                            </button>
                        </div>
                        <div id="dislike_count" style="text-align: center" class="formatted-string style-scope -toggle-button-renderer style-text" aria-label="Hole que tal"><?= $video_info->dislike_count?></div>
                    </a>
                </div>
            </div>

        </div>

    </div>
    <div class="video_actions_wrapper clearfix">
        <div class="video_details_wrapper open">
            <div class="video_details_display_wrapper">
                <div class="video_description_item">
                    Description&nbsp;:
                    <h2 class="video_description_text"><?= $video_info->description ?></h2>
                </div>
                <div class="video_description_item">
                    Main Category&nbsp;:
                    <a href="/channels/<?= $video_info->main_category?>"><?= $video_info->main_category?></a>
                </div>
                <div class="video_description_item">
                    Creator(s)&nbsp;:
                    <a data-creator_id="" href="/creators/<?= $video_info->creator?>.html"><?= $video_info->creator?></a>
                </div>
                <div class="video_description_item">
                    Sent by:
                    <span data-obfs="JTJGam9qb3NsYWJ5JTJGYWN0aXZpdHklMkY=">
                    <?php
                    global $pdo;
                    $query = "SELECT `{$dbName}`.users.username 
                                FROM `{$dbName}`.users 
                                WHERE `{$dbName}`.users.id = ".$video_info->sender_id;
                    $sender = $pdo->query($query)->fetch();
                    echo $sender->username;
                    ?>
                </span>
                </div>
                <div class="video_stats_wrapper clearfix">
                    <div class="video_stats_item pull-left"><div class="video_stats">Duration : <?= $video_info->duration?></div></div>
                    <div class="video_stats_item pull-right"><div class="video_stats">Sent on : <?= $video_info->date_upload?></div></div>
                </div>
            </div>
        </div>
    </div>

    <?= $content ?>


    <script src="<?= getenv('URL_root') . 'src/videos/JS/like_dislike.js' ?>"></script>
    <script>
        (function ($) {

            <?php if (is_array($comments) || is_object($comments)): ?>
            <?php foreach ($comments as $comment):?>
            $('#show_reply_<?= $comment->id?>').on('click',function show_reply(e) {
                e.preventDefault();
                $(this);
                if($('#show_reply_<?= $comment->id?>').text() === "Hide Answer(s)"){
                    $('#show_reply_<?= $comment->id?>').html("<span>Show Answer(s)</span>\n" + "<i class='fas fa-sort-down'></i>");
                    $('.comment_reply_wrapper_<?= $comment->id?>').hide('slow') ;
                }else {
                    $('#show_reply_<?= $comment->id?>').html("<span>Hide Answer(s)</span>" + "<i class='fas fa-sort-up'></i>");
                    $('.comment_reply_wrapper_<?= $comment->id?>').show('slow') ;
                }


            })
            <?php endforeach; ?>
            <?php endif; ?>
        }(jQuery));

        (jQuery)(function ($) {
            <?php if (is_array($comments) || is_object($comments)): ?>
            <?php foreach ($comments as $comment):?>
            $('#comment_reply_input_<?= $comment->id?>').hide();
            <?php if (!isset($comment->replies)): ?>
            $('#show_reply_<?= $comment->id?>').remove();
            <?php endif; ?>
            $('#reply_<?= $comment->id?>').on('click',function reply(e) {
                e.preventDefault();
                <?php if (!isset($comment->replies)): ?>
                document.getElementById('comment_actions_wrapper_<?= $comment->id?>').innerHTML +="<div style='display: block;' id='comment_reply_wrapper_<?= $comment->id ?>' class=\"comment_reply_wrapper\" >\n" +
                    "\t\t\t<div class=\"comment_replies\"></div>\n" +
                    "    <form method='post' action='' id='comment'>"+
                    "    <div id='comment_reply_input_<?= $comment->id?>' class=\"comment_reply_input comment_reply_input_<?= $comment->id?>\">\n" +
                    "        <div class=\"comments_reply_comment_wrapper comments_recaptcha_enabled\">\n" +
                    "            <div id='emojiPickerIconWrap_<?= $comment->id?>' class=\"emojiPickerIconWrap emojiPickerIconWrap_<?= $comment->id?>\">\n" +
                    "                <textarea placeholder='Your Answers' name=\"content\" class=\"comments_new_comment_txt js_new_reply_input emojiPickerInput\" style=\"padding-right: 40px;\"></textarea>\n" +
                    "                <i class=\"far fa-smile-beam\"></i>\n" +
                    "            </div>\n" +
                    "        </div>\n" +
                    <?php if(isset($_SESSION['auth'])): ?>
                    "<div class='comments_new_comment_btn'>"+
                    "<button type='submit' class='js_new_comment  btn_default btn_comments_new' type='submit'>Post</button>"+
                    "</div>"+
                    <?php else: ?>
                    "<div class='comments_new_comment_btn'>"+
                    "<button type='button' class='js_new_comment  btn_default btn_comments_new' type='submit'>Please login to post a comment</button>"+
                    "</div>"+
                    <?php endif; ?>
                    "<input type='hidden' name='parent_id' value='<?= $comment->id?>' id='parent_id_<?= $comment->id?>'>"+
                    "<input type='hidden' name='action' value='comment' >" +
                    "</form>"+
                    "\t\t\t</div>\n" +
                    "\t\t\t\t\t</div>";
                $('#parent_id_<?= $comment->id?>').val(document.getElementById('reply_<?= $comment->id?>').dataset.id);
                <?php else: ?>
                document.getElementById('comment_actions_wrapper_<?= $comment->id?>').innerHTML +="<div id='comment_reply_wrapper_<?= $comment->id ?>' class=\"comment_reply_wrapper\">\n" +
                    "\t\t\t<div class=\"comment_replies\"></div>\n" +
                    "    <form method='post' action='' id='comment'>"+
                    "    <div id='comment_reply_input_<?= $comment->id?>' class=\"comment_reply_input comment_reply_input_<?= $comment->id?>\">\n" +
                    "        <div class=\"comments_reply_comment_wrapper comments_recaptcha_enabled\">\n" +
                    "            <div id='emojiPickerIconWrap_<?= $comment->id?>' class=\"emojiPickerIconWrap emojiPickerIconWrap_<?= $comment->id?>\">\n" +
                    "                <textarea placeholder='Your answer' name=\"content\" class=\"comments_new_comment_txt js_new_reply_input emojiPickerInput\" style=\"padding-right: 40px;\"></textarea>\n" +
                    "                <i class=\"far fa-smile-beam\"></i>\n" +
                    "            </div>\n" +
                    "        </div>\n"+
                    <?php if(isset($_SESSION['auth'])): ?>
                    "<div class='comments_new_comment_btn'>"+
                    "<button type='submit' class='js_new_comment  btn_default btn_comments_new' type='submit'>Post</button>"+
                    "</div>"+
                    <?php else: ?>
                    "<div class='comments_new_comment_btn'>"+
                    "<button type='button' class='js_new_comment  btn_default btn_comments_new' type='submit'>Please login to post a comment</button>"+
                    "</div>"+
                    <?php endif; ?>
                    "<input type='hidden' name='parent_id' value='<?= $comment->id?>' id='parent_id_<?= $comment->id?>'>"+
                    "<input type='hidden' name='action' value='comment' >" +
                    "</form>"+
                    "\t\t\t</div>\n" +
                    "\t\t\t\t\t</div>";
                $('#comment_reply_input_<?= $comment->id?>').hide('slow');
                $('#show_reply_<?= $comment->id?>').html("<span>Show Answer(s)</span>" + "<i class='fas fa-sort-up'></i>");
                $('.comment_reply_wrapper_<?= $comment->id?>').hide('slow') ;
                $('#parent_id_<?= $comment->id?>').val(document.getElementById('reply_<?= $comment->id?>').dataset.id);
                <?php endif;?>
            });
            $('#button_like_comment_<?= $comment->id ?>').click(function (e){
                e.preventDefault();
                vote_comment_<?= $comment->id?>(1);

            });
            $('#button_dislike_comment_<?= $comment->id ?>').click(function (e){
                e.preventDefault();
                vote_comment_<?= $comment->id?>(-1);

            });

            function vote_comment_<?= $comment->id?>(value){
                $.post("<?= getenv('URL_root') . 'src/videos/PHP/liking/like.php' ?>", {
                    ref: $('#button_like_comment_<?= $comment->id ?>').data('ref'),
                    ref_id: $('#button_like_comment_<?= $comment->id ?>').data('ref_id'),
                    user_id: $('#button_like_comment_<?= $comment->id ?>').data('user_id'),
                    vote: value
                }).done(function(data){
                    $('#dislike_count_comment_<?= $comment->id ?>').text(data.dislike_count);
                    $('#like_count_comment_<?= $comment->id ?>').text(data.like_count);
                    $('#button_like_comment_<?= $comment->id ?>').removeClass('is-liked');
                    $('#button_dislike_comment_<?= $comment->id ?>').removeClass('is-disliked');
                    if(data.success) {

                        if (value === 1) {
                            $('#button_like_comment_<?= $comment->id ?>').addClass('is-liked');
                            alert("Your comment like is saved");

                        } else {
                            $('#button_dislike_comment_<?= $comment->id ?>').addClass('is-disliked');
                            alert("Your comment dislike is saved");
                        }
                    } else if(!data.success){
                        if (value=== 1){
                            alert("Your like has been removed");
                        } else {
                            alert("Your dislike has been removed");
                        }
                    }


                }).fail(function (jqXHR){
                    alert(jqXHR.responseText);

                });
            }
            <?php if (isset($comment->replies)): ?>
            <?php foreach ($comment->replies as $key => $reply): ?>


            $('#button_like_reply_<?= $reply->id ?>').click(function (e){
                e.preventDefault();
                vote_reply_<?= $reply->id?>(1);

            });
            $('#button_dislike_reply_<?= $reply->id ?>').click(function (e){
                e.preventDefault();
                vote_reply_<?= $reply->id?>(-1);

            });
            function vote_reply_<?= $reply->id?>(value){
                $.post('<?= getenv('URL_root') . 'src/videos/PHP/liking/like.php' ?>', {
                    ref: $('#button_like_reply_<?= $reply->id ?>').data('ref'),
                    ref_id: $('#button_like_reply_<?= $reply->id ?>').data('ref_id'),
                    user_id: $('#button_like_reply_<?= $reply->id ?>').data('user_id'),
                    vote: value
                }).done(function(data){
                    $('#dislike_count_reply_<?= $reply->id ?>').text(data.dislike_count);
                    $('#like_count_reply_<?= $reply->id ?>').text(data.like_count);
                    $('#button_like_reply_<?= $reply->id ?>').removeClass('is-liked');
                    $('#button_dislike_reply_<?= $reply->id ?>').removeClass('is-disliked');
                    if(data.success) {

                        if (value === 1) {
                            $('#button_like_reply_<?= $reply->id ?>').addClass('is-liked');
                            alert("Your like for the reply comment is saved");

                        } else {
                            $('#button_dislike_reply_<?= $reply->id ?>').addClass('is-disliked');
                            alert("Your dislike for the reply comment is saved");
                        }
                    }else if(!data.success){
                        if (value=== 1){
                            alert("Your like for the reply comment has been removed");
                        } else {
                            alert("Your dislike for the reply comment has been removed");
                        }
                    }


                }).fail(function (jqXHR){
                    alert(jqXHR.responseText);

                });
            }
            <?php endforeach; ?>


            <?php foreach ($comment->replies as $reply): ?>
            $('#reply_<?= $reply->id?>').on('click',function reply(e) {
                e.preventDefault();
                $('#comment_reply_input_<?= $reply->id?>').show('slow');
                $('#parent_id_<?= $comment->id?>').val(document.getElementById('reply_<?= $comment->id?>').dataset.id);
            });



            <?php endforeach; ?>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php endif; ?>
        });
    </script>

</div>

</body>
</html>