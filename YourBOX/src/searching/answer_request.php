<?php
global $pdo, $html_code, $dbName;
if(session_status() === PHP_SESSION_NONE) {
    session_start();
}


require_once getenv('PROJECT_ROOT').'src/inc/Database/db.php';
require_once getenv('PROJECT_ROOT').'src/inc/functions.php';

/**
 * @param $duration_video
 * @param string|null $html_code
 * @return string
 */
function printDurationVideo($duration_video, string|null $html_code): string
{
    $html_code .= "<span class='style-scope ytd-thumbnail-overlay-time-status-renderer' >" . $duration_video . "</span>";
    $html_code .= "</div>";
    $html_code .= "</div>";
    $html_code .= "<div id ='mouseover-overlay' class='style-scope ytd-thumbnail'>";
    $html_code .= "<div id='hover-overlays' class='style-scope ytd-thumbnail'></div>";

    $html_code .= "</div>";
    $html_code .= "</a>";
    $html_code .= "</div>";
    $html_code .= "<div class='text-wrapper style-scope ytd-video-renderer'>";
    $html_code .= "<div id='meta' class='style-scope ytd-video-renderer'>";
    $html_code .= "<div id='title-wrapper' class='style-scope ytd-video-renderer'>";
    $html_code .= "<h3 class='title-and-badge style-scope ytd-video-renderer'>";
    return $html_code;
}

/**
 * @param string|null $html_code
 * @param $id
 * @param string $path_thumbnail
 * @param $duration_video
 * @param $name_video
 * @param $url_video
 * @return string
 */
function getStr(string|null $html_code, $id, string $path_thumbnail, $duration_video, $name_video, $url_video): string
{
    $html_code .= "<div class='style-scope ytd-item-section-renderer thumbnail'>";
    $html_code .= "<div id='dismissable' class='style-scope ytd-video-renderer'>";
    $html_code .= "<div class='thumbnail style-scope ytd-video-renderer' >";
    $html_code .= "<a id='thumbnail' class='sx-simple-endpoint inline-block style-scope ytd-thumbnail' aria-hidden='true' tabindex='-1' rel='null'  href='".getenv('URL_root')."src/videos/PHP/playing/video_page.php?id=" . $id . "'>";
    $html_code .= "<div id='img-shadow' class='style-scope ytd-thumbnail no-transition'  style='background-color: transparent;' >";
    $thumbnail = basename($path_thumbnail);
    $html_code .= "<img id='img' class='style-scope img-shadow' alt='' src='" . getenv('URL_root') . "cache/upload/" . $thumbnail . "' height='138px' width='246'>";
    $html_code .= "</div>";
    $html_code .= "<div id='overlays' class='style-scope ytd-thumbnail'>";
    $html_code .= "<div class='style-scope ytd-thumbnail time' >";
    $html_code = printDurationVideo($duration_video, $html_code);
    $html_code .= "<div class='style-scope ytd-video-renderer'  hidden=''> </div>";
    $html_code .= "<a id='video-title' class='sx-simple-endpoint style-scope ytd-video-renderer' title='" . $name_video . "' href='" . $url_video . "' >";
    $html_code .= "<div style='width: 400px' class='style-scope ytd-video-renderer' >";
    return $html_code;
}

if(isset($_POST["mc"]) && $_POST != ""){

    $each_word=""; $url_video=""; $title_video=""; $j=0;
    $keywords = removeAccents(removeSpecialChars($_POST['mc']));
    $keywords = strtolower(mb_convert_encoding($keywords, 'ISO-8859-1'));
    $words_to_exclude = "with|for|in|";
    $tab_exclusion = explode("|", $words_to_exclude);
    global $req_and, $req_or;
    $req_and ="SELECT * FROM `{$dbName}`.files ";
    $req_or =$req_and;
    for ($i=0; $i<sizeof($tab_exclusion); $i++){
        $keywords= str_replace($tab_exclusion[$i]," ", $keywords);
    }
    $keywords = str_replace("   "," ", str_replace("  "," ", $keywords));
    $keywords = str_replace("-"," ",$keywords);

    $new = false;
    $directory = getenv('PROJECT_ROOT')."cache/searchings/";
    if (!file_exists($directory)) {
        mkdir($directory, 0777, true);
    }
    $new_file = $directory.str_replace(" ","-", $keywords).".txt";
    if(file_exists($new_file)){
        $d1 = strtotime(date("j F Y H:i", filemtime($new_file)));
        $d2 = strtotime(date("j F Y H:i"));
        $delta = (int)$d2 - (int)$d1;
        if($delta > 3600){
            $new = true;
        }
    }else{
        $new = true;
    }


    if($new) {

        if (strlen(str_replace(" ", "", $keywords)) < 3) {
            $_SESSION['flash']['danger'] = "Your search is insufficient to be processed, please rephrase it with more keywords";
            header('Location: '.getenv('URL_root').'index.php');
            exit();
        } elseif (strlen(str_replace(" ", "", $keywords)) > 50) {
            $_SESSION['flash']['danger'] = "Your search is too long to be processed, please rephrase it with less keywords";
            header('Location: '.getenv('URL_root').'index.php');
            exit();
        } elseif (!str_contains($keywords, "a") && !str_contains($keywords, "e") && !str_contains($keywords, "i") && !str_contains($keywords, "o") && !str_contains($keywords, "u") && !str_contains($keywords, "y")) {
            $_SESSION['flash']['danger'] = "Please rephrase your search with vowels.";
            header('Location: '.getenv('URL_root').'index.php');
            exit();
        } else {
            $tab_keywords = explode(" ", $keywords);
            for ($i = 0; $i < sizeof($tab_keywords); $i++) {
                $each_word = rtrim($tab_keywords[$i], "s");
                if (strlen($each_word) > 0) {
                    if ($j == 0) {
                        $req_and .= "WHERE `{$dbName}`.files.name LIKE '%" . $each_word . "%' ";
                        $req_or .= "WHERE `{$dbName}`.files.name LIKE '%" . $each_word . "%' ";
                    } else {
                        $req_and .= "AND `{$dbName}`.files.name LIKE '%" . $each_word . "%' ";
                        $req_or .= "OR `{$dbName}`.files.name LIKE '%" . $each_word . "%' ";
                    }

                    $j++;
                }
            }

            $req_and .= "LIMIT 0,35";
            $j = 0;
            $results = $pdo->query($req_and);
            while ($result = $results->fetch()) {
                $id = $result->id;
                $upload_video=explode(" ", $result -> date_upload);
                $date_upload = $upload_video[0];
                $duration_video = $result-> duration;
                $number_views = $result-> number_of_views;
                if ($number_views == NULL){
                    $number_views = 0;
                }
                $path_thumbnail = $result->URL_preview;
                $path_video = $result->file_url;
                $name_video = $result->name;
                $title_video = stripslashes($name_video);
                $title_video = removeSpecialChars(strtolower($name_video));
                $title_video = str_replace("-", " ", $title_video);
                $url_video = getenv('URL_root') . "src/videos/PHP/playing/video_page.php?id=" .$result->id ;
                for ($i = 0; $i < sizeof($tab_keywords); $i++) {
                    $each_word = rtrim($tab_keywords[$i], "s");
                    if (strlen($each_word) > 1) {
                        $title_video = str_replace($each_word, "<mark>" . $each_word . "</mark>", $title_video);

                    }
                }

                $html_code = getStr($html_code, $id, $path_thumbnail, $duration_video, $name_video, $url_video);
                $html_code .= $title_video;
                $html_code .= "</div>";
                $html_code .= "</a>";
                $html_code .= "</h3>";
                $html_code .= "</div>";
                $html_code .= "<div class='style-scope ytd-video-renderer video-meta-block byline-separated'>";


                $html_code .= "<div id='metadata' class='style-scope ytd-video-meta-block'>";

                $html_code .= "<div id='metadata-line' class='style-scope ytd-video-meta-block'>";
                $html_code .= "<span class='style-scope ytd-video-meta-block'>".$number_views." views</span>";

                $html_code .= "<span class='style-scope ytd-video-meta-block'>".$date_upload."</span>";
                $html_code .= "<dom-repeat strip-whitespace='' class='style-scope ytd-video-meta-block'><template is='dom-repeat'></template></dom-repeat>";
                $html_code .= "</div>";
                $html_code .= "</div>";

                $html_code .= "</div>";
                $html_code .= "</div>";

                $html_code .= "<! --possible to put author or categories of the video description--><div id='description-text' class='style-scope ytd-video-renderer'></div>";

                $html_code .= "<div id='buttons' class='style-scope ytd-video-renderer'></div>";
                $html_code .= "</div>";

                $html_code .= "</div>";
                $html_code .= "</div>";

                $j++;

            }
            if ($j == 0) {
                $html_code = "There are no specific results for your search, but here are some items that might interest you:<br/><br/>";
                $results = $pdo->query($req_or);
                while ($result = $results->fetch()) {
                    $proportion = 0;
                    $id = $result-> id;
                    $number_views = $result-> number_of_views;
                    if ($number_views == NULL){
                        $number_views = 0;
                    }
                    $upload_video=explode(" ", $result -> date_upload);
                    $date_upload = $upload_video[0];
                    $duration_video = $result-> duration;
                    $path_thumbnail = $result -> URL_preview;
                    $thumbnail = basename($path_thumbnail);
                    $path_video = $result-> file_url;
                    $name_video = $result->name;
                    $url_video = getenv('URL_root') . "src/videos/PHP/playing/video_page.php?id=" .$result->id ;
                    $title_video = stripslashes($name_video);
                    $title_video = removeSpecialChars(strtolower($name_video));
                    $title_video = str_replace("-", " ", $title_video);
                    for ($i = 0; $i < sizeof($tab_keywords); $i++) {
                        $each_word = rtrim($tab_keywords[$i], "s");
                        if (str_contains(removeAccents(removeSpecialChars(strtolower($result->name))), $each_word)) {
                            $proportion++;
                        }
                        if (strlen($each_word) > 2) {
                            $title_video = str_replace($each_word, "<mark>" . $each_word . "</mark>", $title_video);
                        }
                    }
                    $proportion = round($proportion / sizeof($tab_keywords), 2);
                    if ($proportion >= 0.5) {
                        $html_code = getStr($html_code, $id, $thumbnail, $duration_video, $name_video, $url_video);
                        $html_code .= "<span style='color: #D8000C;'>" . $proportion * (100) . "% : </span>".$title_video;
                        $html_code .= "</div>";
                        $html_code .= "</a>";
                        $html_code .= "</h3>";
                        $html_code .= "</div>";
                        $html_code .= "<div class='style-scope ytd-video-renderer video-meta-block byline-separated'>";


                        $html_code .= "<div id='metadata' class='style-scope ytd-video-meta-block'>";

                        $html_code .= "<div id='metadata-line' class='style-scope ytd-video-meta-block'>";
                        $html_code .= "<span class='style-scope ytd-video-meta-block'>".$number_views." views</span>";

                        $html_code .= "<span class='style-scope ytd-video-meta-block'>".$date_upload."</span>";
                        $html_code .= "<dom-repeat strip-whitespace='' class='style-scope ytd-video-meta-block'><template is='dom-repeat'></template></dom-repeat>";
                        $html_code .= "</div>";
                        $html_code .= "</div>";

                        $html_code .= "</div>";
                        $html_code .= "</div>";

                        $html_code .= "<! --possible to put author or categories of the video description--><div id='description-text' class='style-scope ytd-video-renderer'></div>";

                        $html_code .= "<div id='buttons' class='style-scope ytd-video-renderer'></div>";
                        $html_code .= "</div>";

                        $html_code .= "</div>";
                        $html_code .= "</div>";
                        $j++;
                        if ($j >= 10) {
                            break;
                        }
                    }

                }

            }
        }
        $cache = fopen($new_file, "w");
        fwrite($cache, $html_code);
        fclose($cache);
    }
    else {
        $cache = fopen($new_file, "r");
        $html_code = fread($cache, filesize($new_file));
        fclose($cache);
        $html_code .= "<div style='float:left; width:100%; padding-top:10px; color: white;'><i>Results of the restitution of the last search</i></div> ";
    }


}
require_once getenv('PROJECT_ROOT').'src/inc/header.php'
?>
<link rel="stylesheet" href="<?= getenv('URL_root').'src/assets/stylesheets/result.css' ?>">
<div  id ="search_results" class="search_results" style="" >
    <strong>List of results</strong><br /><br />
    <div>
        <?php
        if(isset($_POST["mc"]) && $_POST != ""){
            echo $html_code;//."<br/>".$req_and."<br/>".$req_or;
        }
        ?>
    </div>

</div>
