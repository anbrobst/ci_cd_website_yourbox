# Using PHP 8.2 image with Apache
FROM php:8.2-apache

ARG TERRAFORM_OUTPUTS_FILE
ARG WEBAPP_URL
ARG DB_NAME

# Necessary PHP extensions already installed and configured : pdo mbstring fileinfo openssl curl
RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql

# Increasing the upload file size limit of PHP
RUN { \
        echo 'upload_max_filesize = 80G'; \
        echo 'post_max_size = 80G'; \
    } > /usr/local/etc/php/conf.d/upload.ini

# Installation of ffmpeg and jq
RUN apt-get update \
    && apt-get -y full-upgrade \
    && apt-get install -y ffmpeg jq

# Installation of Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Install unzip utility and libs needed by zip PHP extension 
RUN apt-get update \
    && apt-get install --no-cache -y \
    zlib1g-dev \
    libzip-dev \
    unzip
RUN docker-php-ext-install zip

# Copying project files to container
COPY YourBOX/ /var/www/html/yourbox/

# Copying .env.php script and terraform outputs to container
COPY infra/create_env_file.bash /var/www/html/yourbox/create_env_file.bash
COPY $TERRAFORM_OUTPUTS_FILE /var/www/html/yourbox/$TERRAFORM_OUTPUTS_FILE

# Setting up environment variables and PHP configuration
RUN chmod 755 /var/www/html/YourBOX/create_env_file.bash \
    && /var/www/html/YourBOX/create_env_file.bash /var/www/html/YourBOX/$"TERRAFORM_OUTPUTS_FILE" $"DB_NAME" \
    && echo 'auto_prepend_file = "/var/www/html/YourBOX/.env.php"' >> /usr/local/etc/php/conf.d/auto_prepend.ini \
    && rm -rf /var/www/html/YourBOX/create_env_file.bash && rm -rf /var/www/html/YourBOX/$"TERRAFORM_OUTPUTS_FILE"

# Port 80 Exhibition
EXPOSE 80

# Apache configuration
RUN a2enmod rewrite \
    && echo "RewriteEngine On\n\
          RewriteCond %{REQUEST_URI} ^/$\n\
          RewriteRule ^(.*)$ /index.php [L,R=301]" > /var/www/html/YourBOX/.htaccess \
    && echo "<VirtualHost *:80>\n\
                ServerName $WEBAPP_URL\n\
                DocumentRoot /var/www/html/yourbox\n\
                <Directory /var/www/html/yourbox>\n\
                    Options Indexes FollowSymLinks\n\
                    AllowOverride All\n\
                    Require all granted\n\
                </Directory>\n\
            </VirtualHost>" > /etc/apache2/sites-available/YourBOX.conf \
    && a2ensite YourBOX && a2dissite 000-default

# Creating a user for composer
RUN adduser --disabled-password --gecos '' myuser

# Changing the owner of the project files
RUN chown -R myuser:myuser /var/www/html/YourBOX/

USER myuser

# Installation of Composer extensions
RUN composer install --no-dev --working-dir=/var/www/html/YourBOX/ && composer fund --working-dir=/var/www/html/YourBOX/

# Command to start Apache
CMD ["apache2-foreground"]
